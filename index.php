<?php

define('WP_FOLDER_NAME','/wordpress');
define('WP_ROOT', dirname( __FILE__ ) . WP_FOLDER_NAME);

define('WP_USE_THEMES', true);
if ( !defined('ABSPATH') )
	define('ABSPATH', WP_ROOT . '/');
require(WP_ROOT . '/wp-blog-header.php');