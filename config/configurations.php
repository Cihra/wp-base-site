<?php
/**
 * development
 */

define('WP_ALLOW_MULTISITE',false);

define('APP_ROOT',dirname(__DIR__));
define('PUBLIC_ROOT',dirname(__DIR__));

$domainName = 'http://' . getenv('VIRTUAL_HOST');
/** Custom websupport enviroment stuff */
define( 'FORCE_SSL_ADMIN', true ); // Redirect All HTTP Page Requests to HTTPS - Security > Settings > Secure Socket Layers (SSL) > SSL for Dashboard
if($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $_SERVER['HTTPS'] = 'on';
    $_SERVER['SERVER_PORT'] = 443;
    $domainName = 'https://' . getenv('VIRTUAL_HOST');
}
/** End custom websupport enviroment stuff */

define('DOMAIN_NAME', $domainName);

define('WP_HOME',DOMAIN_NAME);
define('WP_SITEURL', WP_HOME.WP_FOLDER_NAME);

define('WP_CONTENT_DIR', APP_ROOT.'/content');
define('WP_CONTENT_URL', DOMAIN_NAME.'/content');
define('WP_PLUGINS_DIR', WP_CONTENT_DIR.'/classes');

define('WP_DEBUG', getenv('ENV_WP_DEBUG'));

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv('DB_DATABASE'));

/** MySQL database username */
define('DB_USER', getenv('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('DB_PASS'));

/** MySQL hostname */
define('DB_HOST', getenv('DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', 'utf8_swedish_ci');

/** CACHE SETTINGS **/
define('CACHE_ALIVE_TIME_TRANSIENT', false);
define('CACHE_ALIVE_TIME_TIMBER', false);

if(WP_DEBUG) {
	ini_set('log_errors', 'On');
	ini_set('display_errors', 1);
	define('WP_DEBUG_DISPLAY', true);
	define('SCRIPT_DEBUG', false);
} else {
	ini_set('display_errors', false);
}

if(WP_ALLOW_MULTISITE) {
	define('MULTISITE', true);
	define('SUBDOMAIN_INSTALL', false);
	define('DOMAIN_CURRENT_SITE', 'dev2.sitefactory.fi');
	define('PATH_CURRENT_SITE', '/domain.fi/');
	define('SITE_ID_CURRENT_SITE', 1);
	define('BLOG_ID_CURRENT_SITE', 1);
}

define('WP_POST_REVISIONS', 5);
define('AUTOMATIC_UPDATER_DISABLED', true );
define('DISALLOW_FILE_EDIT', true);
define('EMPTY_TRASH_DAYS', 30);

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sitewp_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory.
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');*/

/** Sets up WordPress vars and included files. */
//var_dump(file_exists(WP_ROOT . 'wp-settings.php')); die();
//require_once(WP_ROOT . 'wp-settings.php');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CCqs>K@=+=.89Z=tO<hd*d[J^7nI)&.bB?zlOr!/kyT4}SiA;Fm.alj{aieE6(Ow');
define('SECURE_AUTH_KEY',  'BcKUu<Wi]gpU@(B:qT2|n@Em9m&e)LN,gfg+!,D77!!Mq:F~s;z!v+-W{0OMZp5]');
define('LOGGED_IN_KEY',    'ul65ILDlFxuL&u;)}l-6+I E@wG/f2gCvwWwO1uH6V1Vwn-A*%b@)b_4ua-?sl3q');
define('NONCE_KEY',        '_[>*rx-3eAx!l;jo/?k8$_ZcwH,>Yyscs,0:0oL^*5m=@-b!5@2E8o!>b3Yp`/*?');
define('AUTH_SALT',        '=lnA2-6@[~[rvh/v%`l*)d|*oz_5A]x=Q1=!e=Ps2x<LW%I-KN[dPh7CET]4v?Wi');
define('SECURE_AUTH_SALT', ';3NyFRwr3D[dMh!B+?o:MTW=w)-}EB$(r<mDW|.j|{J=>*`Nc^8Z+mi).Rq3o g=');
define('LOGGED_IN_SALT',   '4#XR.s-_?q@RyA<PRQ6<%hsEYMW=V^%M`(7j+L8A?e78!I;LL_GO}~la4]7>=]w?');
define('NONCE_SALT',       'z<1236?DvlOzQ(:e:DA-|{xl:H5HFOuBQg4pQ#%pQrX_)#0>R Itc+`]G2^8Otf_');

/**#@-*/
