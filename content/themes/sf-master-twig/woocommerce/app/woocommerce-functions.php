<?php

use WPClass\WPSite as WPSite;

if ( ! class_exists( 'WooCommerce' ) ) {
  	// woocommerce is not activated
	exit;
}

// Disble admin helper notice
// add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );
// Declare woocommerce theme support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Timber woocommerce support
function timber_set_product( $post ) {
    global $product;
    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}


/* --------------------------
 *
 * theme hooks
 *
** ----------------------- */

	/* tällä tulostetaan kauppa-linkki */
	// add_action('sf_frontpage_after_wide_content', 'sf_print_shop_link');
	if( ! function_exists('sf_print_shop_link') ){
		function sf_print_shop_link(){
			echo '<div class="margin-bottom"><a href="' . esc_url( get_permalink( woocommerce_get_page_id( 'shop' ) ) ) . '" class="shop-button link-button no-margin-bottom no-margin-right">' . __('Show more products', WPSite::$theme_slug) . '</a></div>';
		}
	}

	/* header_login_buttons -> print login / cart buttons */
	add_action('header_login_buttons', 'print_login_account_buttons');
	if( ! function_exists('print_login_account_buttons') ){
		function print_login_account_buttons(){
			echo '<div class="login_buttons_container">';
			if( is_user_logged_in() ){
				echo '<a href="' . esc_url( wc_get_page_permalink( 'myaccount' ) ) . '" class="link-button"><span class="icon icon-user"></span><span class="text">' . __('My account', WPSite::$theme_slug) . '</span></a>';
				echo '<a href="' . esc_url( wc_get_page_permalink( 'cart' ) ) . '" class="link-button"><span class="icon icon-basket"></span><span class="text">' . __('Basket', WPSite::$theme_slug) . '</span></a>';
				echo '<a href="' . esc_url( wp_logout_url( wc_get_page_permalink( 'myaccount' ) ) ) . '" class="link-button"><span class="icon icon-logout"></span><span class="text">' . __('Logout', WPSite::$theme_slug) . '</span></a>';
			}else{
				echo '<a href="' . esc_url( wc_get_page_permalink( 'myaccount' ) ) . '" class="link-button"><span class="icon icon-user"></span><span class="text">' . __('Login', WPSite::$theme_slug) . '</span></a>';
				echo '<a href="' . esc_url( wc_get_page_permalink( 'cart' ) ) . '" class="link-button"><span class="icon icon-basket"></span><span class="text">' . __('Basket', WPSite::$theme_slug) . '</span></a>';
			}
			echo '</div>';
		}
	}

/* --------------------------
 *
 * Shipping packages for local pickup
 * Esimerkki toimitustapojen erittelyyn ostoskorissa
 * Jos valittuna toimitustavaksi "vain myymälästä nouto" , siirretään se omaan toimitusryhmään
 *
** ----------------------- */

	// Hook into shipping packages filter
	// add_filter( 'woocommerce_cart_shipping_packages', 'sf_split_special_shipping_class_items' );
	if( ! function_exists('sf_split_special_shipping_class_items') ){
		function sf_split_special_shipping_class_items( $packages ) {
			$found_item                     = false;
			$special_class                  = 'nouto-myymalasta';
			$new_package                    = current( $packages );
			$new_package['contents']        = array();
			$new_package['contents_cost']   = 0;
			$new_package['applied_coupons'] = array();
			$new_package['ship_via']        = array( 'local_pickup' );
			
			foreach ( WC()->cart->get_cart() as $item_key => $item ) {
				if ( $item['data']->needs_shipping() && $special_class === $item['data']->get_shipping_class() ) {
					$found_item                            = true;
					$new_package['contents'][ $item_key ]  = $item;
					$new_package['contents_cost']         += $item['line_total'];

					// Remove from original package
					$packages[0]['contents_cost'] = $packages[0]['contents_cost'] - $item['line_total'];
					unset( $packages[0]['contents'][ $item_key ] );

					// If there are no items left in the previous package, remove it completely.
					if ( empty( $packages[0]['contents'] ) ) {
						unset( $packages[0] );
					}
				}
			}
			if ( $found_item ) {
			   $packages[] = $new_package;
			}
			return $packages;
		}
	}


/* --------------------------
 *
 * sorting settings
 *
** ----------------------- */

	function sf_wc_remove_sorting_add_settings( $settings ) {
		$new_settings = array();
		foreach ( $settings as $setting ) {
			$new_settings[] = $setting;
			if ( isset( $setting['id'] ) && 'woocommerce_enable_ajax_add_to_cart' === $setting['id'] ) {
				$new_settings[] = array(
						'name'     => __( 'Remove Product Sorting:', WPSite::$theme_slug ),
						'desc_tip' => __( 'Select sorting options to remove from your shop.', WPSite::$theme_slug ),
						'id'       => 'wc_remove_product_sorting',
						'type'     => 'multiselect',
						'class'    => 'chosen_select',
						'options'  => array(
							'menu_order'   	=> __( 'Default Sorting', 'woocommerce' ),
							'popularity'  	=> __( 'Popularity', 'woocommerce' ),
							'rating'  		=> __( 'Average Rating', 'woocommerce' ),
							'date'   		=> __( 'Most Recent', 'woocommerce' ),
							'price'      	=> __( 'Price (Asc)', 'woocommerce' ),
							'price-desc' 	=> __( 'Price (Desc)', 'woocommerce' ),
						),
						'default'  => '',
				);
			}
		}
		return $new_settings;
	}
	add_filter( 'woocommerce_product_settings', 'sf_wc_remove_sorting_add_settings' );

	function sf_remove_sorting_from_settings( $options ) {
		$remove_sorting = get_option( 'wc_remove_product_sorting', array() );
	
		if($remove_sorting){
			foreach( $remove_sorting as $remove ) {
				unset( $options[ $remove ] );
			}
		}
	    
	    return $options;
	}
	add_filter( 'woocommerce_default_catalog_orderby_options', 'sf_remove_sorting_from_settings' );

	function sf_remove_wc_sorting_option( $catalog_orderby_options ) {
		$remove_options = get_option( 'wc_remove_product_sorting', array() );
		if($remove_options){
			foreach( $remove_options as $remove ) {
				unset( $catalog_orderby_options[ $remove ] );
			}
		}
		
		return $catalog_orderby_options;
	}
	add_filter( 'woocommerce_catalog_orderby', 'sf_remove_wc_sorting_option' );

/* --------------------------
 *
 * add new walker to woocommerce menu widget (for dropdown menu)
 *
** ----------------------- */

	add_filter('woocommerce_product_categories_widget_args', 'sf_product_categories_widget_args', 10, 1);
	if( ! function_exists('sf_product_categories_widget_args')){
		function sf_product_categories_widget_args($args) {
		    $args['walker'] = new WPTheme\MenuWalker\Sf_WC_Product_Cat_List_Walker();
		    return $args;
		}
	}

/* --------------------------
 *
 * header small cart
 *
** ----------------------- */

	add_action('header_mini_cart', 'sf_header_cart_widget');

	if ( ! function_exists( 'sf_header_cart_widget' ) ) {
		function sf_header_cart_widget() {
			?>
			<div class="header_cart-container">
				<div class="hover_intent">
					<div class="header_cart-trigger">
						<a class="header_cart-link" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', WPSite::$theme_slug ); ?>">
							<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), WPSite::$theme_slug ), WC()->cart->get_cart_contents_count() ) );?></span>
							<span class="amount">(<?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?>)</span>
						</a>
					</div>
					<div class="header_cart-items">
						<?php the_widget( 'WC_Widget_Cart', 'title=Ostoskori' ); ?>
					</div>
				</div>
			</div>
			<?php
		}
	}
	// ajax päivitys headerin ostoskoriin
	add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
	if( ! function_exists('woocommerce_header_add_to_cart_fragment')){
		function woocommerce_header_add_to_cart_fragment( $fragments ) {
			global $woocommerce;
			
			ob_start();
			
			?>
			<a class="header_cart-link" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', WPSite::$theme_slug ); ?>">
				<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), WPSite::$theme_slug ), WC()->cart->get_cart_contents_count() ) );?></span>
				<span class="amount">(<?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?>)</span>
			</a>
			<?php
			
			$fragments['a.header_cart-link'] = ob_get_clean();
			
			return $fragments;
		}
	}

/* --------------------------
 *
 * new placeholder image
 *
** ----------------------- */

	// add_filter( 'woocommerce_placeholder_img_src', 'sf_wc_placeholder', 10 );
	if( ! function_exists('sf_wc_placeholder')){
		function sf_wc_placeholder( $image_url ) {
		  $image_url = get_stylesheet_directory_uri().'/woocommerce/assets/images/placeholder.png';
		  return $image_url;
		}
	}

/* --------------------------
 *
 * Ninja forms -> tuoteintegraatio
 * Esimerkki tarjouspyyntölomakkeelle tuotetiedot viemisestä
 *
** ----------------------- */

	// Declare custom URL parameters
	// add_filter( 'query_vars', 'sf_add_query_vars_filter' );
	if( ! function_exists('sf_add_query_vars_filter')){
		function sf_add_query_vars_filter( $vars ) {
		  $vars[] = "product-id";
		  $vars[] = "product-url";
		  return $vars;
		}
	}

	// add_filter( 'ninja_forms_before_form_display', 'sf_ninja_displayed_product', 5 );
	if( ! function_exists('sf_ninja_displayed_product')){
		function sf_ninja_displayed_product( $form_id ){
			if ( $form_id == 3 ) {
				$product_permalink = get_query_var('product-url', '#');
				$product_id = get_query_var('product-id', '#');
				if( !empty($product_id) ) {
					$product_name = get_the_title($product_id);
					$product_data = wc_get_product($product_id);
				}
				if( !empty($product_permalink) && !empty($product_name) ) {
					echo '<p>'.__("You are querying product", WPSite::$theme_slug).' <a href="'.$product_permalink.'">'.$product_name.'</a> </p>';
					if( $product_data->short_description ){
						echo '<p>'.$product_data->short_description.'</p>';
					}
					// echo '<p class="price">'.$product_data->get_price().'</p>';
					// echo $product_data->get_price();
					// print_r($product_data);
				}
			}
		}
	}


	// add_filter( 'ninja_forms_display_after_form', 'sf_back_to_product_ninja_link', 5 );
	if( ! function_exists('sf_back_to_product_ninja_link')){
		function sf_back_to_product_ninja_link( $form_id ){
			// print_r($form_id);
			if ( $form_id == 3 ) {
				$product_permalink = get_query_var('product-url');
				if( !empty($product_permalink) ) {
					echo '<a href="'.$product_permalink.'">'.__("Back to product", WPSite::$theme_slug).'</a>';
				}
			}
		}
	}

/* --------------------------
 *
 * WC breadcrumbs separator / home / link
 * Esimerkki murupolun kustomoinnista
 *
** ----------------------- */

	// add_filter( 'woocommerce_breadcrumb_defaults', 'sf_change_breadcrumb_delimiter' );
	if( ! function_exists('sf_change_breadcrumb_delimiter')){
		function sf_change_breadcrumb_delimiter( $defaults ) {
			$defaults['delimiter'] = ' <span class="separator icon-angle-right"></span> ';
			// $defaults['home'] = __( 'Shop', WPSite::$theme_slug );
			return $defaults;
		}
	}


	// add_filter( 'woocommerce_breadcrumb_home_url', 'sf_custom_breadrumb_home_url' );
	if( ! function_exists('sf_custom_breadrumb_home_url')){
		function sf_custom_breadrumb_home_url() {
		    return get_permalink( woocommerce_get_page_id( 'shop' ) );
		}
	}

/* --------------------------
 *
 * My account
 * -poistetaan käytöstä lataukset-osio käyttäjän tiedoista
 *
** ----------------------- */

	// remove_action( 'woocommerce_account_downloads_endpoint', 'woocommerce_account_downloads' );

/* --------------------------
 *
 * Loops
 *
** ----------------------- */

	// remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	// remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
	// remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

	// remove actions
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
	remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

	// add tag filter 
	// add_action( 'woocommerce_before_shop_loop', 'sf_wc_tag_filter', 35 );

	// add sale flash
	// add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_show_product_loop_sale_flash', 10 );

	// add_filter( 'woocommerce_sale_flash', 'sf_filter_woocommerce_sale_flash', 10, 3 ); 

	// add details button
	// add_filter( 'woocommerce_after_shop_loop_item', 'sf_wc_loop_add_readmore_button', 9);

	// remove "add to cart" -text
	// add_filter( 'woocommerce_product_add_to_cart_text', 'sf_wc_product_add_to_cart_text_override', 10 );

	// add image container for product loop thumbs
	add_filter( 'woocommerce_before_shop_loop_item_title', 'sf_wc_open_loop_image_container', 7 );
	add_filter( 'woocommerce_before_shop_loop_item_title', 'sf_wc_close_loop_image_container', 13 );

	// add category for product loop
	add_filter( 'woocommerce_after_shop_loop_item', 'sf_wc_loop_add_product_category', 6 );

	// move title and price outside link elm
	add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_title', 7 );
	add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 8 );
	// add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 8 );

	// add image container for category loop thumbs
	add_filter( 'woocommerce_before_subcategory_title', 'sf_wc_open_loop_image_container', 7 );
	add_filter( 'woocommerce_before_subcategory_title', 'sf_wc_close_loop_image_container', 13 );

	if( ! function_exists('sf_wc_tag_filter')){
		function sf_wc_tag_filter(){
			global $wp_query;


			// print_r($wp_query->queried_object);

			if ( ! $_GET['product_tag'] && ! $_GET['orderby'] && ( 1 === (int) $wp_query->found_posts || ! woocommerce_products_will_display() ) ) {
				return;
			}

			$cat_products = get_posts( $args = array('posts_per_page' => -1, 'offset' => 0, 'category' => '', 'category_name' => '', 'orderby' => 'date', 'order' => 'DESC', 'include' => '', 'exclude' => '', 'meta_key' => '', 'meta_value' => '', 'post_type' => 'product', 'post_mime_type' => '', 'post_parent' => '', 'author' => '', 'author_name' => '', 'post_status' => 'publish', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => $wp_query->queried_object->term_id) ), 'suppress_filters' => true ) );

			$tags = array();
			foreach ($cat_products as $key => $product) {
			    // $wp_query->the_post();
				$prod_terms = get_the_terms( $product->ID, 'product_tag' );
				foreach ($prod_terms as $prod_term) {
					$tags[$prod_term->term_id] = $prod_term;
				}
			}

			if ( !empty( $tags ) ) {
				?>
					<form class="woocommerce-tag-ordering" method="get">
						<select name="product_tag" class="product_tag">
							<?php
								echo '<option value="">'.__("No filter", WPSite::$theme_slug).'</option>';
							    foreach ($tags as $tag ) {
							    	if( $tag->slug == $_GET['product_tag'] ){ 
							   			echo '<option selected="selected" value="'. $tag->slug .'">';
							    	}else{
							   			echo '<option value="'. $tag->slug .'">';
							    	}
							   		echo  __("Filter", WPSite::$theme_slug) . ': ' . $tag->name .'</option>';
							    }
							?>
						</select>
					</form>
					<script>jQuery(function(o){ o(".woocommerce-tag-ordering").on("change","select.product_tag",function(){o(this).closest("form").submit()}) });</script>
				<?php
			}
		}
	}

	if( ! function_exists('sf_wc_loop_add_readmore_button')){
		function sf_wc_loop_add_readmore_button(){
			global $product;
			// if ( $product && $product->is_purchasable() && $product->is_in_stock() ){}
			if ( $product ) {
				echo '<a href="'.get_permalink($product->id).'" data-product_id="'.$product->id.'" class="button details">'.__('Details', WPSite::$theme_slug).'</a>';
			}
		}
	}
	if( ! function_exists('sf_wc_product_add_to_cart_text_override')){
		function sf_wc_product_add_to_cart_text_override(){
			return '';
		}
	}
	if( ! function_exists('sf_wc_open_loop_image_container')){
		function sf_wc_open_loop_image_container(){
			echo '<span class="loop-image-container">';
		}
	}	
	if( ! function_exists('sf_wc_close_loop_image_container')){
		function sf_wc_close_loop_image_container(){
			echo '</span>';
		}
	}
	if( ! function_exists('sf_wc_loop_add_product_category')){
		function sf_wc_loop_add_product_category(){
			global $product;
			$prod_cat_ids = $product->get_category_ids();
			if ( $prod_cat_ids[0] ) {
				$product_cat = get_term_by( 'id', $prod_cat_ids[0], 'product_cat' );
				echo '<div class="product-loop-prod-cat"><a href="'. get_term_link($product_cat->term_id, 'product_cat'). '">'. $product_cat->name .'</a></div>';
			}
		}
	}
	if( ! function_exists('sf_filter_woocommerce_sale_flash')){
		function sf_filter_woocommerce_sale_flash( $span_class_onsale_esc_html_sale_woocommerce_span, $post, $product ) { 
		    return '<span class="onsale">' . esc_html__( 'Sale', WPSite::$theme_slug ) . '</span>'; 
		}; 
	}

/* --------------------------
 *
 * Single Product
 *
** ----------------------- */

	add_action( 'woocommerce_single_product_summary', 'sf_display_sku', 2 );
	add_filter( 'woocommerce_single_product_image_thumbnail_html', 'sf_single_product_image_thumbnail_html', 10, 2 ); 

	// Descriptionia varten pitää tehä tällänen
	add_filter( 'woocommerce_product_tabs', 'sf_custom_description_tab', 98 );

	// custom quote
	// add_action( 'woocommerce_single_product_after_content', 'sf_wc_single_add_quote_button', 32 );

	// show cross-sell products in single-template
	// add_action( 'woocommerce_after_single_product_summary', 'sf_show_cross_sell_products', 10);

	/**
	 * remove Reviews
	 *
	 */
	// remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );
	// remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
	// remove_action( 'woocommerce_review_meta', 'woocommerce_review_display_meta', 10 );
	// remove_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', 10 );

	/**
	 * Content Wrappers.
	 *
	 * @see woocommerce_output_content_wrapper()
	 * @see woocommerce_output_content_wrapper_end()
	 */
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

	if( ! function_exists('sf_wc_single_add_quote_button')){
		function sf_wc_single_add_quote_button(){
			global $product;
			// $price_html = $product->get_price_html();
			$prod_id = $product->get_id();
			if ( $prod_id ) {
				$show_quote_button = get_post_meta( $prod_id, 'show_quote_button', true );

				// if ( $product && $product->stock_status == 'instock' && $product && $product->regular_price ) {
				if ( $product && $show_quote_button ) {
					echo '<a href="'.get_permalink(347).'?product-id='.$prod_id.'&product-url='.get_permalink($prod_id).'" class="link-button-alt request-quote">'.__('Request a quote', WPSite::$theme_slug).'</a>';
				}else{
					return false;
				}
			}
		}
	}

	if( ! function_exists('sf_single_product_image_thumbnail_html')){
		function sf_single_product_image_thumbnail_html( $html, $thumbnail_id ){
			// Add lightbox link in gallery img
			$a_html = str_replace('<a href', '<a rel="gallery" class="lightbox woocommerce-gallery-lightbox" href', $html);
			return $a_html;
		}
	}

	if( ! function_exists('sf_display_sku')){
		function sf_display_sku(){
		    global $product;
		    if( $product->get_sku() ){
		    	echo '<b class="product-sku">' . $product->get_sku() . ' </b>';
		    }
		}
	}

	if( ! function_exists('sf_show_cross_sell_products')){
		function sf_show_cross_sell_products(){
		    $crosssells = get_post_meta( get_the_ID(), '_crosssell_ids',true);
		    if( ! empty( $crosssells ) ){
			    $args = array( 
			        'post_type' => 'product', 
			        'posts_per_page' => -1, 
			        'post__in' => $crosssells 
			        );
			    $products = new WP_Query( $args );
			    if( $products->have_posts() ) : 
			        echo '<div class="cross-sells"><h2>'.__('Related products', WPSite::$theme_slug).'</h2>';
			        woocommerce_product_loop_start();
			        while ( $products->have_posts() ) : $products->the_post();
			            wc_get_template_part( 'content', 'product' );
			        endwhile; // end of the loop.
			        woocommerce_product_loop_end();
			        echo '</div>';
			    endif;
			    wp_reset_query();
		    }else{ return; }
		}
	}

	if( ! function_exists('sf_custom_description_tab')){
		function sf_custom_description_tab( $tabs ) {
			global $product;
			$prod_desc = $product->get_description();
			if( ! empty($prod_desc) ){
				$tabs['description']['callback'] = 'sf_custom_description_tab_content';	// Custom description callback
			}else{
				unset( $tabs['description'] );
			}
			return $tabs;
		}
	}
	if( ! function_exists('sf_custom_description_tab_content')){
		function sf_custom_description_tab_content() {
			global $product;
			echo '<h2 class="product-description-heading">' . __('Description', WPSite::$theme_slug) . '</h2>';
			$content = $product->get_description();
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		}
	}

/* --------------------------
 *
 * Cart
 * -Poistetaan ostoskorista cross-sell tuotteet
 *
** ----------------------- */

	// remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
