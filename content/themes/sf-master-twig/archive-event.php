<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['title'] = __('Upcoming events', WPClass\WPSite::$theme_slug);

$args_events = array(
	'posts_per_page' 	=> -1,
	'post_type'      	=> 'event',
	'orderby'   		=> 'meta_value_num',
	'meta_key'  		=> 'event_date',
	'order' 			=> 'ASC',
	'meta_query' 		=> array(
		'relation' 		=> 'OR',
		array(
			'key'     => 'event_date',
			'value'   => current_time('timestamp'),
			'compare' => '>=',
		),
		array(
			'key'     => 'event_date_end',
			'value'   => current_time('timestamp'),
			'compare' => '>=',
		)
	),
	'paged'				=> false
);

$args_old_events = array(
	'posts_per_page' 	=> -1,
	'post_type'      	=> 'event',
	'orderby'   		=> 'meta_value_num',
	'meta_key'  		=> 'event_date',
	'order' 			=> 'DESC',
	'meta_query' 		=> array(
		'relation' 		=> 'AND',
		array(
			'key'     => 'event_date',
			'value'   => current_time('timestamp'),
			'compare' => '<',
		),
		array(
			'key'     => 'event_date_end',
			'value'   => current_time('timestamp'),
			'compare' => '<',
		),
		'relation' 		=> 'OR',
		array(
			'key'     => 'event_date',
			'value'   => current_time('timestamp'),
			'compare' => '<',
		)
	),
	'paged'				=> false
);

$context['posts'] = Timber::get_posts($args_events); 
// $context['old_posts'] = Timber::get_posts($args_old_events); 

$templates = array( 'archive.twig', 'index.twig' );

array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
Timber::render( $templates, $context, WPClass\WPSite::$default_template_cache_alive_time );
