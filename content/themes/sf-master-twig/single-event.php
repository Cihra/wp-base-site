<?php
/**
 * The Template for displaying single-event posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if( $post->cmb_event_group && is_array($post->cmb_event_group) ){
	if( array_key_exists('0', $post->cmb_event_group) ){
		foreach ($post->cmb_event_group as $key => $value) {
			$post->cmb_event_group[$key] = unserialize($value);
		}
	}else{
		$post->cmb_event_group[0] = $post->cmb_event_group;
	}
}

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context, WPClass\WPSite::$default_template_cache_alive_time );
}
