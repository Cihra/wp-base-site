<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'taxonomy-product_category.twig', 'index.twig' );

$data = Timber::get_context();

$data['term_page'] = new TimberTerm();

$data['title'] = $data["term_page"];

$data['posts'] = Timber::get_posts();

Timber::render( $templates, $data, WPClass\WPSite::$default_template_cache_alive_time );
