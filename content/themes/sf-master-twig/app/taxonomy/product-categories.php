<?php

// use WPClass\WPSite as WPSite;

add_action('init', function() {
	$labels = array(
		'name'					=> _x( 'Tuotekategoria', 'Taxonomy plural name' ),
		'singular_name'			=> _x( 'Tuotekategoria', 'Taxonomy singular name' ),
		'search_items'			=> __( 'Etsi tuotekategorioita' ),
		'popular_items'			=> __( 'Suosittuja tuotekategorioita' ),
		'all_items'				=> __( 'Kaikki tuotekategoriat' ),
		'parent_item'			=> __( 'Isäntäkategoria' ),
		'parent_item_colon'		=> __( 'Isäntäkategoria' ),
		'edit_item'				=> __( 'Muokkaa tuotekategoriat' ),
		'update_item'			=> __( 'Päivitä tuotekategoriat' ),
		'add_new_item'			=> __( 'Lisää tuotekategoria' ),
		'new_item_name'			=> __( 'Lisää tuotekategoria' ),
		'add_or_remove_items'	=> __( 'Lisää tai poista tuotekategoria' ),
		'menu_name'				=> __( 'Tuotekategoria' )
	);
	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => __('product_category') ),
		'query_var'         => true,
	);

	register_taxonomy( __('product_category'), 'product', $args);
	register_taxonomy_for_object_type(__('product_category'), 'product');
});

?>