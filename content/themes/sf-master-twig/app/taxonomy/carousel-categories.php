<?php
/**
 * Name: carousel-categories
 * 
 * @since 2018-10
 * @author Matu
 *
 */

add_action('init', function() {

	$content_type_slug = 'slide';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> __( $cat_plural, $domain ),
		'singular_name'			=> __( $cat_singular, $domain ),
		'menu_name'				=> __( $cat_plural, $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => $cat_tax_name ),
		'query_var'         => true,
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});

?>