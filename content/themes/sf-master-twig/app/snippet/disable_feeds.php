<?php
	/*
	* Disable feeds
	*/
	add_action( 'wp_loaded', 'remove_links' );
	add_action( 'template_redirect', 'filter_feeds', 1 );
	add_filter( 'bbp_request', 'filter_bbp_feeds', 9 );

	function remove_links() {
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
	}

	function filter_feeds() {
		if( !is_feed() || is_404() )
			return;

		if( $this->allow_main()
			&& ! ( is_singular() || is_archive() || is_date() || is_author() || is_category() || is_tag() || is_tax() || is_search() ) )
			return;

		$this->redirect_feed();
	}

	//BBPress feed detection sourced from bbp_request_feed_trap() in BBPress Core.
	function filter_bbp_feeds( $query_vars ) {
		// Looking at a feed
		if ( isset( $query_vars['feed'] ) ) {

			// Forum/Topic/Reply Feed
			if ( isset( $query_vars['post_type'] ) ) {

				// Matched post type
				$post_type = false;

				// Post types to check
				$post_types = array(
					bbp_get_forum_post_type(),
					bbp_get_topic_post_type(),
					bbp_get_reply_post_type()
				);

				// Cast query vars as array outside of foreach loop
				$qv_array = (array) $query_vars['post_type'];

				// Check if this query is for a bbPress post type
				foreach ( $post_types as $bbp_pt ) {
				    if ( in_array( $bbp_pt, $qv_array, true ) ) {
					    $post_type = $bbp_pt;
					    break;
				    }
				}

				// Looking at a bbPress post type
				if ( ! empty( $post_type ) ) {
					$this->redirect_feed();
				}
			}
			// @todo User profile feeds
		}

		// No feed so continue on
		return $query_vars;
	}

?>
