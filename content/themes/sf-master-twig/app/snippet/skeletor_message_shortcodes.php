<?php
/**
 * @author J3
 * @since 2016-03-31
 * 
 * [skeletor_row_start]
 * [skeletor_col class="col-m-12"]
 *      First column content here
 * [skeletor_col class="col-m-12"]
 *      Second column content here
 * [skeletor_row_end]
 * 
 */ 

add_shortcode('skeletor_note', array('Skeletor_note','start') );
add_shortcode('skeletor_note_end', array('Skeletor_note','end') );

Class Skeletor_note {
    
    static $row_opened = false;

    //Start column row
    public static function start($args = array()) {
        $classes = 'message-note';
        if(isset($args['class']))
            $classes = ' '.(string)$args['class'];
        return '<div class="'.$classes.'">';
    }

    //End column row
    public static function end($args = array()) {
        
        self::$row_opened = false;
        return '</div></div>';
    }
}