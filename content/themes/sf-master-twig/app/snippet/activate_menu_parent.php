<?php

//Activate parent menu elements
add_filter('wp_get_nav_menu_items', 'custom_nav_menu_items', 10,2);
function custom_nav_menu_items( $items, $menu ){

  global $post;

  if($post->post_type == 'post') {

  // $categories = get_post_meta( $post->ID, 'product_categories', true );
  	$categories = array();
 	$terms = get_the_terms($post->ID, 'category');
 	
   foreach($terms as $term)
   		$categories[] = $term->term_id;
   
  }

  if(!empty($categories)){

	   // var_dump($categories);
	   foreach($items as $key => $item) {
	    // var_dump( in_array($item->object_id, $categories) );
	   	// var_dump($item);
	    if( in_array( $item->object_id, $categories) ) {
	     // var_dump($item); die();
	     $items[$key]->classes[] = 'current-menu-item';
	     $items[$key]->current = true;
	     if( intval($item->menu_item_parent) ) {
	      $items = _mark_nav_menu_item_active_recursive($items, $item->menu_item_parent);
	     } else
	      $items[$key]->current_item_parent = true;
	    }
	   }
  }

   return $items;
 }

 function _mark_nav_menu_item_active_recursive( $items, $object_id ){
  if( intval($object_id) ) {
   foreach($items as $key => $item) {
    // echo $item->object_id.",".$object_id;
    // var_dump($object_id);
    if( $item->ID == $object_id ) {
     $items[$key]->classes[] = 'current-menu-item';
     $items[$key]->current = true;
     $items[$key]->current_item_parent = true;
      if( intval($items[$key]->menu_item_parent) )
        $items = _mark_nav_menu_item_active_recursive($items, $item->menu_item_parent);
      else {
        $items[$key]->classes[] = 'post-direct-parent';
      }
    }
   }
  }

  return $items;
 }

 ?>