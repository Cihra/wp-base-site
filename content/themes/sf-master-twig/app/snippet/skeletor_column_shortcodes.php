<?php
/**
 * @author J3
 * @since 2016-03-31
 * 
 * [skeletor_row_start]
 * [skeletor_col class="col-m-12"]
 *      First column content here
 * [skeletor_col class="col-m-12"]
 *      Second column content here
 * [skeletor_row_end]
 * 
 */ 

add_shortcode('skeletor_row_start', array('Skeletor_Shortcode','skeletor_start_row') );
add_shortcode('skeletor_row_end', array('Skeletor_Shortcode','skeletor_end_row') );
add_shortcode('skeletor_col', array('Skeletor_Shortcode','skeletor_add_column') );

Class Skeletor_Shortcode {
    
    static $row_opened = false;

    //Start column row
    public static function skeletor_start_row($args = array()) {
        $classes = '';
        if(isset($args['class']))
            $classes = ' '.(string)$args['class'];
        return '<div class="row'.$classes.'">';
    }

    //End column row
    public static function skeletor_end_row($args = array()) {
        
        self::$row_opened = false;
        return '</div></div>';
    }

    //Start new column and if not first column, close the previous one
    public static function skeletor_add_column($args = array()) {
        
        $ret = '';

        $attr = shortcode_atts(array(
            'class' => 'col-xs-24'
        ), $args);

        if(!self::$row_opened) {
            self::$row_opened = true;
        } else
            $ret .= '</div>';

        $ret .= '<div class="'.$attr['class'].'">';

        return $ret;
    }
}
?>