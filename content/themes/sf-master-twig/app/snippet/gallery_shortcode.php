<?php 
// Kuvagalleria shortcode
if( !function_exists('sf_image_gallery') ){
	function sf_image_gallery( $atts ) {
		$a = shortcode_atts( array(
		    'kategoria' => '',
		    'tyyli' => ''
		), $atts );

	    if( !empty($a['kategoria']) ){

		    $images = get_posts( array( 'post_type'=>'attachment', 'post_status' => 'inherit', 'orderby' => 'menu_order', 'order' => 'ASC', 'post_mime_type' => 'image/jpeg,image/gif,image/jpg,image/png', /*'post_parent' => null,*/ 'posts_per_page' => -1, 'tax_query' => array( array('taxonomy' => 'media_category', 'field' => 'slug', 'terms' => $a['kategoria']) ) ) );

				// print_r($images);

			if( !empty($images) ){
				
				wp_enqueue_script( 'gallery-grid' );

			    $html = '<div class="cf"></div><div class="image-list responsive-grid cf">';
				    $html .= '<div class="grid-sizer"></div>';
				    $html .= '<div class="gutter-sizer"></div>';
			    	foreach( $images as $image ):
			    		$image_thumb = wp_get_attachment_image_src( $image->ID, array(500,0) );
			    		$image_large = wp_get_attachment_image_src( $image->ID, array(1500,0) );
			    		// $image_original = wp_get_attachment_image_src( $image->ID, 'gallery-original' );
			    		if( !empty($image_large) && !empty($image_thumb) ){
			    			$size_class = 'width--1';
			    			if( $image_large[1] / $image_large[2] > 2 ){ 
			    				/* kuva on yli 2x leveämpi kuin mitä se on korkea */ 
			    				$size_class = 'width--4'; 
			    			}else if( $image_large[1] / $image_large[2] > 1.5 ){
								/* kuva on yli 1.5x leveämpi kuin mitä se on korkea */ 
								$size_class = 'width--3'; 
							}else if( $image_large[1] / $image_large[2] > 1 ){
								/* kuva on leveämpi kuin mitä se on korkea */ 
								$size_class = 'width--2'; 
							}else if( $image_large[1] / $image_large[2] < 1 ){
								/* kuva on korkeampi kuin mitä se on leveä */ 
								$size_class = 'width--1'; 
							}else if( $image_large[1] / $image_large[2] < .5 ){
								/* kuva on yli 2x korkeampi kuin mitä se on leveä */ 
								$size_class = 'width--1 height--2'; 
			    			}else{
			    				$size_class = 'width--1';
			    			}
				    		$html .= '<div class="grid-item '.$size_class.'">';
					    		$html .= '<a class="lightbox grid-item-image-link" rel="'.$a['kategoria'].'" href="'.$image_large[0].'">';
					    			$html .= '<img class="grid-item-image" src="'.$image_thumb[0].'" alt="'.$image->post_title.'">';
					    			if( !empty($image->post_excerpt) ){
					    				$html .= '<span class="grid-item-content">'.$image->post_excerpt.'</span>';
					    			}
					    		$html .= '</a>';
				    		$html .= '</div>';
			    		}
			    	endforeach;
			    $html .= '</div>';

		    }else{
		    	$html = '<div class="image-list-empty cf"></div>';
		    }
		}else{
			$html = '<div class="image-list-empty no-category cf"></div>';
		}

		return $html;
	}
}
add_shortcode( 'kuvagalleria', 'sf_image_gallery' );

?>