<?php 
// Henkilöstö shortcode
function staff_list( $atts ) {
	$a = shortcode_atts( array(
	    'kategoria' => ''
	), $atts );

    $args = array('post_type' => 'person', 'posts_per_page' => -1, 'post_status' => 'publish', 'orderby' => 'menu_order');

	if( $a['kategoria'] ){
    	$args['tax_query'] = array( array( 'taxonomy' => 'person_category', 'field' => 'slug', 'terms' => $a['kategoria'] ) );
	}
	$staff = get_posts($args);

    $html = '<div class="staff-list">';
	    $html .= '<div class="row no-margin padding-gutter flex">';
	    	foreach( $staff as $person ):
	    		$html .= '<div class="col-xs-24 col-m-12 flex flex-stretch">';
		    		$html .= '<div class="person flex-grow">';
		  				$html .= '<div class="row no-margin padding-gutter">';
			    			$html .= '<div class="col-xs-24 person-image">';
	    						if( has_post_thumbnail($person->ID) ){
	    						$html .= get_the_post_thumbnail($person->ID);
			    				//$html .= '<p class="wp-caption-text">'.get_post(get_post_thumbnail_id($person->ID))->post_excerpt.'</p>';
	    						/*$html .= '<a href="'.get_the_permalink($person->ID).'">'.get_the_post_thumbnail($person->ID).'</a>';*/
			    				}else{
			    				// $html .= '<img src="'.get_stylesheet_directory_uri().'/assets/img/placeholder.png">';
			    				}
				    		$html .= '</div>';
				    		$html .= '<div class="col-xs-24 person-data">';
					    		$html .= '<b class="person-title">'.$person->post_title.'</b>';
					    		$html .= '<div class="person-content">'.wpautop($person->post_content).'</div>';
				    		$html .= '</div>';
		    			$html .= '</div>';
		    		$html .= '</div>';
	    		$html .= '</div>';
	    	endforeach;
		$html .= '</div>';
    $html .= '</div>';

    return $html;
}
add_shortcode( 'henkilöstö', 'staff_list' );

?>