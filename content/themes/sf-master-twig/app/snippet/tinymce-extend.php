<?php

use WPClass\WPSite as WPSite;

add_filter('mce_buttons_2', 'my_mce_buttons_2');
add_filter( 'tiny_mce_before_init', 'button_styles_drop' );

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'button_styles_drop' ) ) {
    function button_styles_drop( $settings ) {

        // Create array of new styles
        $new_styles = array(
            array(
                'title' => __( 'Painikkeet / Linkit', WPSite::$theme_slug ),
                'items' => array(
                    array(
                        'title'     => __('Painike', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'link-button',
                    ),
                    array(
                        'title'     => __('Painike alt', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'link-button-alt',
                    ),
                    array(
                        'title'     => __('Linkki nuolella (ennen)', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'icon-right-open',
                    ),
                    array(
                        'title'     => __('Linkki nuolella (jälkeen)', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'icon-right-open-after',
                    ),
                    array(
                        'title'     => __('Sijainti-ikoni', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'icon-location',
                    ),
                    array(
                        'title'     => __('Kännykkä-ikoni', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'icon-mobile',
                    ),
                    array(
                        'title'     => __('Kirjekuori-ikoni', WPSite::$theme_slug),
                        'selector'  => 'a,i,span,em',
                        'classes'   => 'icon-mail-1',
                    )
                ),
            ),
            array(
                'title' => __( 'Teemakohtaiset muotoilut', 'Sitefactory' ),
                'items' => array(
                    array(
                        'title'     => __('Ingressi-teksti', WPSite::$theme_slug),
                        'selector'  => 'p',
                        'classes'   => 'ingress',
                    )
                ),
            ),
        );

        // Merge old & new styles
        $settings['style_formats_merge'] = true;

        // Add new styles
        $settings['style_formats'] = json_encode( $new_styles );

        // Return New Settings
        return $settings;

    }
}

?>