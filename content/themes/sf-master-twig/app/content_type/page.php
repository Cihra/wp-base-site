<?php
/**
 * Name: Sivun lisäkentät
 * 
 * @since 2018-02-12
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

namespace WPClass;

if( isset($_GET['post']) ){ $post_id = $_GET['post']; }elseif( isset($_POST['post_ID']) ){ $post_id = $_POST['post_ID']; }else{ $post_id = false; }
if( $post_id ){ $page_template = get_post_meta( $post_id, '_wp_page_template', true ); }else{ $page_template = false; }

if($page_template == 'page-front.php') {
	// Etusivu
	MetaBoxes::register_meta_boxes(
		array(
		    'title' => 'Palvelunostot',
		    'pages' => 'page',
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    	    'id'   => 'cmb_service_box_top_content', 
		    	    'name' => 'Palvelunostojen (yläosan) teksti', 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 8
		    	    ),
		    	    'cols' => 12,
		    	),
		    	array( 
		    		'id'   => 'cmb_service_group', 
		    		'name' => 'Kolumnit', 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => 'Lisää rivi',
		    		'string-delete-field' => 'Poista rivi',
		    		'cols' => 12,
		    		'fields' => array(
		    			array( 
		    			    'id'   => 'cmb_service_box_image_1', 
		    			    'name' => 'Noston kuvake', 
		    			    'type' => 'image', 
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_image_2', 
		    			    'name' => 'Noston kuvake', 
		    			    'type' => 'image', 
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_image_3', 
		    			    'name' => 'Noston kuvake', 
		    			    'type' => 'image', 
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_content_1', 
		    			    'name' => 'Noston teksti', 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_content_2', 
		    			    'name' => 'Noston teksti', 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_content_3', 
		    			    'name' => 'Noston teksti', 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 4,
		    			),
		    		)
		    	),
		    	array( 
		    	    'id'   => 'cmb_service_box_bottom_content', 
		    	    'name' => 'Palvelunostojen (alaosan) teksti', 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 6
		    	    ),
		    	    'cols' => 12,
		    	),
		    ),
		)
	);
}else{

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Pääkuva'),
	        'pages' => array('page'),
	        'context'    => 'normal',
	        'priority'   => 'high',
	        'fields' => array(
		        array(
		            'id'   => 'top_bg_image', 
		            'name' => 'Yläosan iso pääkuva', 
		            'desc' => 'Tähän tulee teemaan valittu oletuskuva, jos ei ole valittuna.',
		            'type' => 'image', 
		            'size' => 'height=110&width=300&crop=1',
		            'cols' => 12,
		        )
	   		)
	    )
	);

	MetaBoxes::register_meta_boxes(
		array(
		    'title' => 'Lisäkentät',
		    'pages' => 'page',
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    		'id'   => 'cmb_service_group', 
		    		'name' => 'Kolumnit', 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => 'Lisää rivi',
		    		'string-delete-field' => 'Poista rivi',
		    		'cols' => 12,
		    		'fields' => array(
		    			array( 
		    			    'id'   => 'cmb_service_box_content_1', 
		    			    'name' => 'Noston teksti', 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 6,
		    			),
		    			array( 
		    			    'id'   => 'cmb_service_box_content_2', 
		    			    'name' => 'Noston teksti', 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 6,
		    			)
		    		)
		    	),
		    	array( 
		    	    'id'   => 'cmb_service_box_bottom_content', 
		    	    'name' => 'Kolumnien alle teksti', 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 6
		    	    ),
		    	    'cols' => 12,
		    	),
		    ),
		)
	);
}
