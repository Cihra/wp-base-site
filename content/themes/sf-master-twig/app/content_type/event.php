<?php
/**
 * Name: Tapahtuma
 * 
 * @since 2018-02-12
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

//This is the machine name of the content type
$content_type_machine_name = 'event';
$content_type_display_name = 'Tapahtuma';

//Create dynamically the content type
$content_module = new ContentType(
    //Post type name
    $content_type_machine_name, 
    //Post type options
    array(
        'singular_name' => $content_type_display_name,
        'rewrite' => array( 'slug' => __('tapahtuma') ),
        'supports' => array('title','excerpt','editor','thumbnail'),
        'has_archive' => true
    ),
    //Post type labels
    array(
        'name' => __('Tapahtumat'),
        'singular_name' => $content_type_display_name,
        'add_new' => __('Lisää uusi'),
        'add_new_item' => __('Lisää uusi tapahtuma'),
        'edit_item' => __('Muokkaa tapahtumaa'),
        'new_item' => __('Uusi tapahtuma'),
        'view_item' => __('Näytä tapahtuma'),
        'all_items' => __('Kaikki tapahtumat'),
        'archives' => __('Tapahtuma-arkisto')
    ),
    //Post type meta boxes and fields
    array(
        'title' => 'Tapahtuman tiedot',
        'pages' => array($content_type_machine_name),
        'context'    => 'normal',
        'priority'   => 'high',
        'has_archive' => true,
        'fields' => array(
        	array(
        	    'id'   => 'event_location', 
        	    'name' => __('Tapahtuman sijainti'), 
        	    'type' => 'text',
        	    'cols' => 3,
        	),
            array(
                'id'   => 'event_date', 
                'name' => __('Tapahtuman päivämäärä'), 
                'type' => 'date_unix',
                'cols' => 3,
            ),
            array(
                'id'   => 'event_date_end', 
                'name' => __('Tapahtuman loppumis-päivämäärä'), 
                'type' => 'date_unix',
                'cols' => 3,
            )
        )
    )
);
MetaBoxes::register_meta_boxes(
    array(
        'title' => 'Tapahtuman lisätiedot',
        'pages' => array($content_type_machine_name),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
        	array( 
        		'id'   => 'cmb_event_group', 
        		'name' => 'Ohjelmakuvaukset', 
        		'type' => 'group',
        		'repeatable' => true,
        		'sortable' => true,
        		'string-repeat-field' => 'Lisää ohjelmakuvaus',
        		'string-delete-field' => 'Poista ohjelmakuvaus',
        		'cols' => 12,
        		'fields' => array(
        			array( 
        			    'id'   => 'cmb_event_box_title', 
        			    'name' => 'Ohjelman otsikko', 
        			    'type' => 'text', 
        			    'cols' => 12,
        			),
        			array( 
        			    'id'   => 'cmb_event_box_image', 
        			    'name' => 'Ohjelman kuva', 
        			    'desc' => '(Vapaaehtoinen) ohjelmakuva. Suositeltu kuvan koko n. 850 x 300px',
        			    'size' => 'height=230&width=400&crop=1',
        			    'type' => 'image', 
        			    'cols' => 4,
        			),
        			array( 
        			    'id'   => 'cmb_event_personnel_alt', 
        			    'name' => 'Ohjelman puhuja/esittäjä lisätiedot', 
        				'desc' => 'Tähän voit syöttää käsin lisätietoja esittäjästä.', 
        			    'type' => 'wysiwyg', 
        			    'options' => array(
        			    	'textarea_rows' => 7
        			    ),
        			    'cols' => 5,
        			),
        			array( 
        				'id'       => 'cmb_event_personnel', 
        				'name'     => 'Ohjelman puhuja/esittäjä', 
        				'desc'     => 'Jos valitset tästä henkilön, henkilön yhteystiedot lisätään automaattisesti järjestelmästä ohjelmaan.', 
        				'type'     => 'post_select', 
        				'cols' => 3,
        				'use_ajax' => true,
        				'query' => array( 
        					'post_type' => 'person'
        				)
        			),
        			array( 
        			    'id'   => 'cmb_event_box_content', 
        			    'name' => 'Ohjelman kuvaus', 
        			    'type' => 'wysiwyg', 
        			    'options' => array(
        			    	'textarea_rows' => 9
        			    ),
        			    'cols' => 12,
        			),
        		),
        	)
        )
    )
);


?>