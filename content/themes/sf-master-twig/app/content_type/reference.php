<?php
/**
 * Name: reference
 * 
 * @since 2017-07-21
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

// This is the machine name of the content type
$content_type_name = 'Referenssi';
$content_type_name_alt = 'referenssiä';
$content_type_name_plural = 'Referenssit';
$content_type_name_plural_alt = 'referenssejä';
$content_type_slug = 'reference';
$content_type_slug_rewrite = 'referenssi';

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 
    
    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'singular_name' => $content_type_name,
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes' ),
        'has_archive' => true,
        'exclude_from_search' => false
    ),
    // Post type labels
    array(
        'menu_name'             => $content_type_name_plural,
        'name'                  => $content_type_name,
        'name_admin_bar'        => $content_type_name,
        'add_new'               => 'Lisää uusi',
        'add_new_item'          => 'Lisää uusi '.$content_type_name,
        'new_item'              => 'Uusi '.$content_type_name,
        'edit_item'             => 'Muokkaa '.$content_type_name_alt,
        'view_item'             => 'Näytä '.$content_type_name,
        'all_items'             => 'Kaikki '.strtolower($content_type_name_plural),
        'search_items'          => 'Etsi '.$content_type_name_plural_alt,
        'parent_item_colon'     => 'Vanhemmat '.strtolower($content_type_name_plural).':',
        'not_found'             => strtolower($content_type_name_plural_alt).' ei löytynyt.',
        'not_found_in_trash'    => strtolower($content_type_name_plural_alt).' ei löytynyt roskakorista.'
    )
);

MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Lisäasetukset'),
        'pages' => 'reference',
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array( 
                'id'   => 'additional_content', 
                'name' => 'Listaussivun sisältö', 
                'desc' => 'Tulee näkyviin referenssilistauksiin kuvan viereen',
                'type' => 'wysiwyg', 
            ),
            array( 
                'id'   => 'content_image', 
                'name' => 'Lisäkuva/listauskuva', 
                'desc' => 'Listauksessa näkyvä kuva.',
                'type' => 'image', 
            )
        )
    )
);


?>