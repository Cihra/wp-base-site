<?php
/**
 * Name: person
 * 
 * @since 2018-10
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Person', $domain);
$content_type_name_plural = __('Personnel', $domain);
$content_type_slug = 'person';
$content_type_slug_rewrite = __('person', $domain);

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array('editor', 'title', 'thumbnail'),
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'query_var'           => false
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    )

);

MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Contact information', $domain),
        'pages' => $content_type_slug,
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array( 
                'id'   => 'cmb_phone', 
                'name' => __('Phone number', $domain), 
                'type' => 'text', 
            ),
            array( 
                'id'   => 'cmb_mobile', 
                'name' => __('Mobile phone number', $domain), 
                'type' => 'text', 
            ),
            array( 
                'id'   => 'cmb_email', 
                'name' => __('Email', $domain), 
                'type' => 'text', 
            )
        )
    )
);

MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Additional information', $domain),
        'pages' => $content_type_slug,
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array( 
                'id'   => 'cmb_title', 
                'name' => __('Title', $domain), 
                'type' => 'text', 
            ),
            array( 
                'id'   => 'cmb_job', 
                'name' => __('Job / task', $domain), 
                'type' => 'text', 
            )
        )
    )
);

?>