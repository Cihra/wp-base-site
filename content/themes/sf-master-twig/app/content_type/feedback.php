<?php
/**
 * Name: Palaute
 * 
 * @since 2018-02-12
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

//This is the machine name of the content type
$content_type_machine_name = 'feedback';
$content_type_display_name = 'Palaute';

//Create dynamically the content type
$content_module = new ContentType(
    //Post type name
    $content_type_machine_name, 
    //Post type options
    array(
        'singular_name' => $content_type_display_name,
        'rewrite' => array( 'slug' => __('palaute') ),
        'supports' => array('title','excerpt','editor','thumbnail'),
        'has_archive' => false
    ),
    //Post type labels
    array(
        'name' => __('Palautteet'),
        'singular_name' => $content_type_display_name,
        'add_new' => __('Lisää uusi'),
        'add_new_item' => __('Lisää uusi Palaute'),
        'edit_item' => __('Muokkaa Palautetta'),
        'new_item' => __('Uusi Palaute'),
        'view_item' => __('Näytä Palaute'),
        'all_items' => __('Kaikki Palautteet'),
        'archives' => __('Palaute-arkisto')
    )
);

?>