<?php 
/**
 * Name: Page columns
 * 
 * @since 2019-12-11
 * @author Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

namespace WPClass;
$domain = 'sitefactory-twig';

// 2-columns
	MetaBoxes::register_meta_boxes(
		array(
		    'title' => __('Additional fields 2-columns', $domain),
		    'pages' => 'page',
		    'hide_on' => array( 'page-template' => array( 'page-front.php' ) ),
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    	    'id'   => 'cmb_alternative_columns', 
		    	    'name' => __('Change columns to 40% - 60%', $domain), 
		    	    'type' => 'checkbox', 
		    	    'cols' => 12,
		    	),
		    	array( 
		    		'id'   => 'cmb_cols_2_group', 
		    		'name' => __('50% - 50% columns', $domain), 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => __('Add row', $domain),
		    		'string-delete-field' => __('Delete row', $domain),
		    		'cols' => 12,
		    		'fields' => array(
		    			array( 
		    			    'id'   => 'cmb_cols_2_content_1', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 6,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_2_content_2', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 8
		    			    ),
		    			    'cols' => 6,
		    			)
		    		)
		    	),
		    	array( 
		    	    'id'   => 'cmb_cols_2_bottom_content', 
		    	    'name' => __('Text below columns', $domain), 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 6
		    	    ),
		    	    'cols' => 12,
		    	),
		    ),
		)
	);

// 3-columns
	MetaBoxes::register_meta_boxes(
		array(
		    'title' => __('Additional fields 3-columns', $domain),
		    'pages' => 'page',
		    'hide_on' => array( 'page-template' => array( 'page-front.php' ) ),
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    		'id'   => 'cmb_cols_3_group', 
		    		'name' => __('33% - 33% - 33% columns', $domain), 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => __('Add row', $domain),
		    		'string-delete-field' => __('Delete row', $domain),
		    		'cols' => 12,
		    		'fields' => array(
		    			// array( 
		    			//     'id'   => 'cmb_cols_3_image_1', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 4,
		    			// ),
		    			// array( 
		    			//     'id'   => 'cmb_cols_3_image_2', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 4,
		    			// ),
		    			// array( 
		    			//     'id'   => 'cmb_cols_3_image_3', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 4,
		    			// ),
		    			array( 
		    			    'id'   => 'cmb_cols_3_content_1', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_3_content_2', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 4,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_3_content_3', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 4,
		    			),
		    		)
		    	),
		    	array( 
		    	    'id'   => 'cmb_cols_3_bottom_content', 
		    	    'name' => __('Text below columns', $domain), 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 6
		    	    ),
		    	    'cols' => 12,
		    	),
		    ),
		)
	);

// 4-columns
	MetaBoxes::register_meta_boxes(
		array(
		    'title' => __('Additional fields 4-columns', $domain),
		    'pages' => 'page',
		    'hide_on' => array( 'page-template' => array( 'page-front.php' ) ),
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    		'id'   => 'cmb_cols_4_group', 
		    		'name' => __('25% - 25% - 25% - 25% columns', $domain), 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => __('Add row', $domain),
		    		'string-delete-field' => __('Delete row', $domain),
		    		'cols' => 12,
		    		'fields' => array(
		    			// array( 
		    			//     'id'   => 'cmb_cols_4_image_1', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 3,
		    			// ),
		    			// array( 
		    			//     'id'   => 'cmb_cols_4_image_2', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 3,
		    			// ),
		    			// array( 
		    			//     'id'   => 'cmb_cols_4_image_3', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 3,
		    			// ),
		    			// array( 
		    			//     'id'   => 'cmb_cols_4_image_4', 
		    			//     'name' => __('Column image', $domain), 
		    			//     'type' => 'image', 
		    			//     'cols' => 3,
		    			// ),
		    			array( 
		    			    'id'   => 'cmb_cols_4_content_1', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 3,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_4_content_2', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 3,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_4_content_3', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 3,
		    			),
		    			array( 
		    			    'id'   => 'cmb_cols_4_content_4', 
		    			    'name' => __('Column Textarea', $domain), 
		    			    'type' => 'wysiwyg', 
		    			    'options' => array(
		    			    	'textarea_rows' => 6
		    			    ),
		    			    'cols' => 3,
		    			)
		    		)
		    	),
		    	array( 
		    	    'id'   => 'cmb_cols_4_bottom_content', 
		    	    'name' => __('Text below columns', $domain), 
		    	    'type' => 'wysiwyg', 
		    	    'options' => array(
		    	    	'textarea_rows' => 6
		    	    ),
		    	    'cols' => 12,
		    	),
		    ),
		)
	);
	
?>