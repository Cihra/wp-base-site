<?php 

namespace WPTheme\MenuWalker;
use Walker_Nav_Menu as Walker_Nav_Menu;

class Sub extends Walker_Nav_Menu {
	  
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		    // depth dependent classes
		    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
		    $classes = array(
		        'sub-menu',
		        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
		        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
		        'menu-level-' . $display_depth
		        );
		    $class_names = implode( ' ', $classes );
		  
		    // build html
		    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
		}
		
		// add main/sub classes to li's and links
		function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		    global $wp_query;
		    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
		  
		    // depth dependent classes
		    $depth_classes = array(
		        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
		        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
		        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
		        'level-' . $depth
		    );
		    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
		  
		    // passed classes
		    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
		    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
		  
		    // build html
		    $output .= $indent . '<li id="sub-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
		  
		    // link attributes
		    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
		  
		    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
		        $args->before,
		        $attributes,
		        $args->link_before,
		        apply_filters( 'the_title', $item->title, $item->ID ),
		        $args->link_after,
		        $args->after
		    );
		  
		    // build html
		    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}



	// filter_hook function to react on sub_menu flag
	public function filter_sub_menu( $sorted_menu_items, $args ) {

		if(isset($args->walker))
			$walker = $args->walker;

		if(!$walker instanceof Sub)
			return $sorted_menu_items;

	//if ( isset( $args->sub_menu ) ) {
	    $root_id = 0;
	    
	    // find the current menu item
	    foreach ( $sorted_menu_items as $menu_item ) {
	      if ( $menu_item->current ) {
	        // set the root id based on whether the current menu item has a parent or not
	        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
	        break;
	      }

	      //J3/Häx 2017-06-30: Koska postit eivät ole menussa, niin tämän filtterin vuoksi menu-hack ei toimi. Snippeteistä löytyy activate_menu_parent.php -file, 
	      //jossa tämä luokka asetetaan lähimmälle parentille
	      elseif( in_array('post-direct-parent', $menu_item->classes) ) {
	          $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
	           break;
	      }

	    }
	    
	    // find the top level parent
	    if ( ! isset( $args->direct_parent ) ) {
	      $prev_root_id = $root_id;
	      while ( $prev_root_id != 0 ) {
	        foreach ( $sorted_menu_items as $menu_item ) {
	          if ( $menu_item->ID == $prev_root_id ) {
	            $prev_root_id = $menu_item->menu_item_parent;
	            // don't set the root_id to 0 if we've reached the top of the menu
	            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
	            break;
	          } 
	        }
	      }
	    }

	    $menu_item_parents = array();
	    foreach ( $sorted_menu_items as $key => $item ) {
	      // init menu_item_parents
	      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;

	      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
	        // part of sub-tree: keep!
	        $menu_item_parents[] = $item->ID;
	      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
	        // not part of sub-tree: away with it!
	        unset( $sorted_menu_items[$key] );
	      }
	    }
	    
	    return $sorted_menu_items;
	/*} else {
	    return $sorted_menu_items;
	  }*/

	  
	}

	public function render_navigation() {

		add_filter( 'wp_nav_menu_objects', array($this, 'filter_sub_menu'), 10, 2 );

		$ret = '';

		$args = array(
			'theme_location' => 'submenu',
			'depth' => 0,
			'echo' => false,
			'fallback_cb' => false,
			'sub_menu' => true,
			'walker' => $this
		);	
		
		$subpages = wp_nav_menu( $args );

		if($subpages) {
			$ret .= '<nav id="subnav" class="vertical margin-bottom">';
			$ret .= $subpages;
			$ret .= '</nav>';
		}

		return $ret;
	}
}
