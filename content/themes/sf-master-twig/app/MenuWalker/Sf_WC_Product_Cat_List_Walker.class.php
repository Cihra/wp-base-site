<?php 
namespace WPTheme\MenuWalker;
use WC_Product_Cat_List_Walker as WC_Product_Cat_List_Walker;

// lisätään woocommercen menu walkeriin open submenu napit
class Sf_WC_Product_Cat_List_Walker extends WC_Product_Cat_List_Walker {
	public $tree_type = 'product_cat';
	public $db_fields = array(
		'parent' => 'parent',
		'id'     => 'term_id',
		'slug'   => 'slug',
	);


	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		if ( 'list' != $args['style'] ) {
			return;
		}

		$indent = str_repeat( "\t", $depth );
		$output .= "$indent<span class='open-submenu'></span><ul class='children'>\n";
	}

}

 ?>