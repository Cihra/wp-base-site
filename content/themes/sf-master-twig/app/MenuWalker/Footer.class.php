<?php

namespace WPTheme\MenuWalker;
use Walker_Nav_Menu as Walker_Nav_Menu;
use WPClass\WPSite as WPSite;

class Footer extends Walker_Nav_Menu {

	function render_navigation($depth = 0) {
		$ret = '';

		$defaults = array(
			'theme_location' => 'footermenu',
			'depth' => $depth,
			'fallback_cb' => false,
			'echo' => false,
			'walker' => $this
		);
		$ret = wp_nav_menu( $defaults );

		return $ret;
	}
}
?>