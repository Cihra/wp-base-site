<?php

namespace WPTheme\MenuWalker;
use Walker_Nav_Menu as Walker_Nav_Menu;
use WPClass\WPSite as WPSite;

class Mobile extends Walker_Nav_Menu {

	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '<div class="clear"></div>'.'</li>';
	}

	function start_lvl( &$output, $depth = 0, $args = array() ){
    	$output .= '<span class="open-submenu"><span class="symbol"><span class="line-horizontal"></span><span class="line-vertical"></span></span></span><div class="clear"></div><ul class="sub-menu">';
	}

	function render_navigation() {
		$ret = '';

		$defaults = array(
			'theme_location' => 'mobilemenu',
			'depth' => 0,
			'fallback_cb' => false,
			'echo' => false,
			'walker' => $this
		);
		$ret = wp_nav_menu( $defaults );

		return $ret;
	}
}
?>