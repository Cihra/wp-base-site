<?php
/**
 * Something wordpress should include by default ..widget for Report Archive :)
 * 
 */

use WPClass\WidgetBootstrap as WidgetBootstrap;

class PostArchive extends WidgetBootstrap {

	function __construct() {

		//Set defaults
		$this->widget_name = 'Post Archive';
		$this->widget_description = 'Select a taxonomy and term where to show posts';

		//Run the widget initialization
		parent::__construct();

		//Add fields
		$this->add_field(
			'count',
			array(
				'description' => 'Number of posts', 
				'default_value' => 2, 
				'type' => 'text'
			)
		);
		
		$a_taxonomies = array();
		foreach( get_taxonomies(array(),'objects') as $_taxonomy ) {
			$a_taxonomies[$_taxonomy->name] = $_taxonomy->label;
		}
		$this->add_field(
			'taxonomy',
			array(
				'name' => 'taxonomy', 
				'description' => 'Please select taxonomy', 
				'default_value' => '', 
				'type' => 'select',
				'options' => $a_taxonomies
			)
		);

		$this->add_field(
			'term',
			array(
				'description' => 'Term',
				'type' => 'select'
			)
		);
	}


	/* Customize the backend of the widget */
	public function form( $instance ) {

		//Taxonomy must be set before we can show the term dropdown
		if( !empty($instance['taxonomy']) ) {

			$a_terms = array();
			$termarray = get_terms(array(
				'taxonomy' => $instance['taxonomy'],
				'order' => 'ASC',
				'hide_empty' => false
			));
			foreach( $termarray as $_term ) {
				$a_terms[$_term->slug] = $_term->name;
			}
			$this->add_field(
				'term',
				array(
					'description' => 'Term', 
					'default_value' => 'Please select term', 
					'type' => 'select',
					'options' => $a_terms
				)
			);
		}

		parent::form($instance);
	}


	/* Customize frontend of the widget */
	public function widget($args, $instance) {

		//Add data
		$query = array(
			'posts_per_page' => $instance['count']
		);

		if($instance['taxonomy'] && $instance['term']) {
			$query['tax_query'] = array(
				array(
					'taxonomy' => $instance['taxonomy'],
					'field'    => 'slug',
					'terms'    => $instance['term']
				)
			);
		}

		$this->add_context('posts', Timber::get_posts($query));

		parent::widget($args, $instance);
	}

}

new PostArchive();
?>