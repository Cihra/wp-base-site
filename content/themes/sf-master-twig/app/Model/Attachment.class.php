<?php
/**
 * Helper class for handling attachments in WP
 *  
 * @since 2016-02-11
 * @author J3
 */

namespace WPClass;

Class Attachment extends WPObject {

	static $_post_type = 'attachment';

	/**
	 * Return file size of the attachment
	 */
	public function getFileSize() {

		$size = filesize(get_attached_file($this->getId()));

		$units = array( 'b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		return number_format($size / pow(1024, $power), 2, '.', ',') . $units[$power];
	}

	/**
	 * Return thumbnail of image
	 * 
	 */
	public function thumbnail($size = null, $icon = false, $attr = '') {
		return wp_get_attachment_image( $this->getId(), $size, $icon, $attr );
	}
}

?>