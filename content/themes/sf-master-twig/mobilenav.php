<?php
/**
 * The Template for displaying mobilenav
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'mobilenav.twig' ), $data, WPClass\WPSite::$default_template_cache_alive_time );
