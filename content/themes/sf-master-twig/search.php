<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'index.twig' );

global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}

$context = Timber::get_context();

$context['is_search'] = is_search();
$context['title'] = __('Search results for', WPClass\WPSite::$theme_slug).': '. get_search_query();
// $context['wrapper_class'] = 'narrow';
$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context );