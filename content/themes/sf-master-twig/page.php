<?php
/**
 * The template for displaying all pages.
 *
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Kolumnien nouto
if( $post->cmb_service_group && is_array($post->cmb_service_group) ){
	if( array_key_exists('0', $post->cmb_service_group) ){
		foreach ($post->cmb_service_group as $key => $value) {
			$post->cmb_service_group[$key] = unserialize($value);
		}
	}else{
		$a_temp = array();
		$a_temp[0] = $post->cmb_service_group;
		$post->cmb_service_group = $a_temp;
	}
}

Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context, WPClass\WPSite::$default_template_cache_alive_time );