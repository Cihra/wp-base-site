<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive.twig', 'index.twig' );

global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}

$context = Timber::get_context();

$context['title'] = __('Archive', WPClass\WPSite::$theme_slug);
if ( is_day() ) {
	$context['title'] = __('Archive: ', WPClass\WPSite::$theme_slug).get_the_date( 'D M Y' );
} else if ( is_month() ) {
	$context['title'] = __('Archive: ', WPClass\WPSite::$theme_slug).get_the_date( 'M Y' );
} else if ( is_year() ) {
	$context['title'] = __('Archive: ', WPClass\WPSite::$theme_slug).get_the_date( 'Y' );
} else if ( is_tag() ) {

	$queried_object = get_queried_object();
	$context['archive_tax'] = $queried_object->taxonomy;
	$context['archive_term'] = $queried_object->slug;
	$context['archive_name'] = $queried_object->name;
	$context['title'] = single_tag_title( '', false );

} else if ( is_category() ) {

	$context['title'] = single_cat_title( '', false );
	$queried_object = get_queried_object();
	$context['archive_tax'] = $queried_object->taxonomy;
	$context['archive_term'] = $queried_object->slug;
	$context['archive_name'] = $queried_object->name;
	array_unshift( $templates, 'archive-' . $queried_object->taxonomy . '.twig' );

} else if ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context, WPClass\WPSite::$default_template_cache_alive_time );
