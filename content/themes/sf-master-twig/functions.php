<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}
Timber::$dirname = array('templates', 'views');

/**
 * Load textdomain
 * @author Matu
*/
// Textdomain to match site class theme_slug
load_theme_textdomain( 'sitefactory-twig', get_template_directory() . '/languages' );

/**
 * Include theme assets, classes and content types
 * @author J3
 * @todo autoloader + namespaces
 */ 
$a_include_rules = array(
	'/app/snippet/*.php',
	'/app/content_type/*.php',
	'/app/taxonomy/*.php',
	'/app/widget/*.php'
);
// foreach($a_include_rules as $rule) {
// 	if($rule) {
// 		foreach(glob(get_template_directory().$rule) as $filename) {
// 			include_once($filename);
// 		}
// 	}
// }
// Child theme rules
foreach($a_include_rules as $rule) {
	if($rule) {
		foreach(glob(get_stylesheet_directory().$rule) as $filename) {
			include_once($filename);
		}
	}
}

/**
 * Include woocommerce-functions php
 * @author Matu
 */ 
if( class_exists( 'WooCommerce' ) ) {
	include_once('woocommerce/app/woocommerce-functions.php');
}

/**
 * Extends WPSite class - all theme related additions go here
 * @author J3
 */ 

class Site extends WPClass\WPSite {

	public function __construct() {

		self::$theme_slug = 'sitefactory-twig';

		parent::__construct();

		$theme_header_settings = array(
			'default-image'          => false,
			'width'                  => 2560,
			'height'                 => 390,
			'flex-height'            => true,
			'flex-width'             => true,
			'uploads'                => true,
			'random-default'         => false,
			'header-text'            => false,
			'default-text-color'     => '',
			'wp-head-callback'       => '',
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
		);
		add_theme_support( 'custom-header', $theme_header_settings );

		//Thumbnail sizes
		$thumbnail_sizes = array();
		$thumbnail_sizes[] = array(
			'name' 		=> 'original',
			'width' 	=> 1024,
			'height' 	=> 800,
			'crop' 		=> false
		);

		register_nav_menu( 'mainmenu', __( 'Main menu', self::$theme_slug ) );
		register_nav_menu( 'submenu', __( 'Sub menu', self::$theme_slug ) );
		register_nav_menu( 'mobilemenu', __( 'Mobile menu', self::$theme_slug ) );
		register_nav_menu( 'footermenu', __( 'Footer menu', self::$theme_slug ) );

		add_filter( 'timber_context', array( $this, 'add_to_context' ), 10 );
		add_filter( 'timber_context', array( $this, 'add_main_menu_to_context' ), 80 );
		add_filter( 'timber_context', array( $this, 'add_footer_menu_to_context' ), 80 );
		add_filter( 'timber_context', array( $this, 'add_sub_menu_to_context' ), 80 );
		add_filter( 'timber_context', array( $this, 'add_mobile_menu_to_context' ), 80 );
		add_action( 'wp_enqueue_scripts', array($this,'load_assets') );
		add_action( 'widgets_init', array($this, 'unregister_default_widgets'), 10 );

		$this->set_thumbnail_sizes($thumbnail_sizes);
		$this->register_sidebars();

	}

	public function unserialize_field_group($group){
		// usage: {% set field_group = site.unserialize_field_group(group) %}
		if($group === false){
			return false;
		}
		if( is_array($group) && !empty($group) ){
			if( array_key_exists('0', $group) ){
				foreach ($group as $key => $value) {
					$group[$key] = unserialize($value);
				}
			}else{
				$a_temp = array();
				foreach ($group as $key => $value) {
					if( !empty( $value ) ){
						$group[$key] = $value;
					}else{
						unset($group[$key]);
					}
				}
				$a_temp[0] = $group;
				$group = $a_temp;
			}
		}
		if( array_key_exists('0', $group) && empty( $group[0] ) ){
			return false;
		}
		return $group;
	}

	//Variables for twig in global context
	public function add_to_context( $context ) {
	
		$context['site'] = $this;

		$context['is_user_logged_in'] = is_user_logged_in();
		$context['logout_link'] = wp_logout_url( get_permalink() );

		$context['theme_header_image'] = get_header_image();
		
		return $context;
	}

	public function add_main_menu_to_context( $context ) {

		//main navigation
		$context['menu_main'] = new WPTheme\MenuWalker\Main();
		
		return $context;
	}

	public function add_footer_menu_to_context( $context ) {

		//footer navigation
		$context['menu_footer'] = new WPTheme\MenuWalker\Footer();
		
		return $context;
	}

	public function add_sub_menu_to_context( $context ) {

		//sub navigation
		$menu_sub = new WPTheme\MenuWalker\Sub();
		$context['sub_menu_html'] = $menu_sub->render_navigation();
		
		return $context;
	}

	public function add_mobile_menu_to_context( $context ) {

		//mobile navigation
		$mobile_navigation = new WPTheme\MenuWalker\Mobile();
		$context['menu_mobile'] = $mobile_navigation->render_navigation();
		
		return $context;
	}

	public function load_assets() {
		// Register scripts
		// die(get_stylesheet_directory_uri());
		wp_register_script( 'sitefactory-grid', get_template_directory_uri() . '/assets/lib/skeletor-lib/js/skeletor-main.js', array('jquery'), filemtime( get_template_directory() . '/assets/lib/skeletor-lib/js/skeletor-main.js' ), true );
		wp_register_script( 'owl-carousel', get_template_directory_uri() . '/assets/lib/owl-carousel/owl.carousel.min.js', false, filemtime( get_template_directory() . '/assets/lib/owl-carousel/owl.carousel.min.js' ), true );
		wp_register_script( 'sitefactory-twig', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), filemtime( get_template_directory() . '/assets/js/main.js' ), true );
		// This is loaded in gallery shortcode
		wp_register_script( 'gallery-grid', get_template_directory_uri() . '/assets/js/gallery.js', array('jquery'), filemtime( get_template_directory() . '/assets/js/gallery.js' ), true );

		// Enque styles
		// wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700|Roboto:300,400,500,700', false, '2.0.0' );
		wp_enqueue_style( 'sitefactory-grid', get_template_directory_uri() . '/assets/lib/skeletor-lib/css/skeletor-main.css', false, filemtime( get_template_directory() . '/assets/lib/skeletor-lib/css/skeletor-main.css' ) );
		// wp_enqueue_style( 'sitefactory-twig', get_template_directory_uri() . '/assets/css/styles.css', array('sitefactory-grid'), filemtime( get_template_directory() . '/assets/css/styles.css' ) );
		wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/fontawesome.css', array(), filemtime( get_template_directory() . '/assets/css/fontawesome.css' ) );
		
		// Enque scripts
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'sitefactory-grid' );
		wp_enqueue_script( 'owl-carousel' );
		wp_enqueue_script( 'sitefactory-twig' );
	}

	public function register_sidebars() {

		register_sidebar(array (
			'name' => __( 'Header content', parent::$theme_slug ),
			'id' => 'header-content',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

		register_sidebar(array (
			'name' => __( 'Sidebar', parent::$theme_slug ),
			'id' => 'sidebar',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

		if( class_exists( 'WooCommerce' ) ) {
			// Kaupan sivupalkki
			register_sidebar(array (
				'name' => __( 'Shop Sidebar', parent::$theme_slug ),
				'id' => 'shop_sidebar',
				'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
				'after_widget' => "</div>",
				'before_title' => '<h2 class="shop-sidebar-heading">',
				'after_title' => '</h2>',
			));
		}

		register_sidebar(array (
			'name' => __( 'Footer content', parent::$theme_slug ),
			'id' => 'footer-content',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

		register_sidebar(array (
			'name' => __( 'Mobilenav top', parent::$theme_slug ),
			'id' => 'mobilenav-top',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

		register_sidebar(array (
			'name' => __( 'Mobilenav bottom', parent::$theme_slug ),
			'id' => 'mobilenav-bottom',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

		register_sidebar(array (
			'name' => __( 'SOME buttons', parent::$theme_slug ),
			'id' => 'some',
			'before_widget' => '<div class="some-widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<span class="hide">',
			'after_title' => '</span>',
		));

	}

	// unregister widgets 
	public function unregister_default_widgets() {
		unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Text');
		unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
		// jätetään vielä nää, voipi joskus tulla tarpeen
		// unregister_widget('WP_Widget_Search');
		// unregister_widget('WP_Nav_Menu_Widget');
	}

}
$site = new Site();
