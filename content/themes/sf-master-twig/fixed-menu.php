<?php
/**
 * The Template for displaying fixed menu
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'fixed-menu.twig' ), $data, WPTheme\WPSite::$default_template_cache_alive_time );
