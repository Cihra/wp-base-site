<?php
/**
 * The Template for displaying subnav
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'subnav.twig' ), $data, WPClass\WPSite::$default_template_cache_alive_time );