/**

	- Skeletor [main.js] -

	@file main.js
	@author Sitefactory

	Rivit 12 ja 14 estävät konfliktin jos Prototype ladattu

*/

jQuery.noConflict();

(function($)
{
	// Enabloidaan strict mode
	"use strict";

	/*!
	 * hoverIntent v1.8.0 // 2014.06.29 // jQuery v1.9.1+
	 * http://cherne.net/brian/resources/jquery.hoverIntent.html
	 *
	 * You may use hoverIntent under the terms of the MIT license. Basically that
	 * means you are free to use hoverIntent as long as this header is left intact.
	 * Copyright 2007, 2014 Brian Cherne
	 */
	(function($){$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var cfg={interval:100,sensitivity:6,timeout:0};if(typeof handlerIn==="object"){cfg=$.extend(cfg,handlerIn)}else{if($.isFunction(handlerOut)){cfg=$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector})}else{cfg=$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut})}}var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if(Math.sqrt((pX-cX)*(pX-cX)+(pY-cY)*(pY-cY))<cfg.sensitivity){$(ob).off("mousemove.hoverIntent",track);ob.hoverIntent_s=true;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=false;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=$.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type==="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).on("mousemove.hoverIntent",track);if(!ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).off("mousemove.hoverIntent",track);if(ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}})(jQuery);


	    $("nav#main.dropdown").hoverIntent({
	        over: function(){
		    	/* Aukaise nykyinen droppari */
		    	$(this).children('ul.sub-menu').fadeIn(150).addClass('show');
	    	},
	        out: function(){
		    	/* Piilota vanha valikko */
		    	$(this).children('ul.sub-menu.show').fadeOut(150).removeClass('show');
	    	},
	        selector: 'li.menu-item-has-children',
	        timeout: 400
	    });

        $(".header_cart-container").hoverIntent({
            over: function(){
    	    	$(this).children('.header_cart-items').slideDown(100).addClass('show');
        	},
            out: function(){
    	    	$(this).children('.header_cart-items.show').slideUp(100).removeClass('show');
        	},
            selector: '.hover_intent',
            timeout: 400
        });

	    /* Sidebar menu toggle */
        $('#subnav ul.menu li.menu-item-has-children[class*="current"]').addClass('open').children('ul.sub-menu').slideDown(150);
        $('#subnav ul.menu li.menu-item-has-children>span.open-submenu').on('click', function(e){
			var $_this = $(this), $_parent_li = $_this.parent(), $_parent_ol = $_this.parent('ul');

			if ( $_parent_li.children('ul.sub-menu').length ){
				if( $_parent_li.hasClass('open') ){
					$_parent_li.removeClass('open'); 
					$_parent_li.children('ul.sub-menu').slideUp(150); 
				}else{
					$_parent_li.addClass('open'); 
					$_parent_li.children('ul.sub-menu').slideDown(150); 
				} 
			} 
		}); 

	    /* Sidebar product-menu toggle */
	    $('#sidebar ul.product-categories li.cat-parent[class*="current"]').addClass('open').children('ul.children').slideDown(150);
	    $('#sidebar ul.product-categories li.cat-parent>span.open-submenu').on('click', function(e){
			var $_this = $(this), $_parent_li = $_this.parent(), $_parent_ol = $_this.parent('ul');

			if ( $_parent_li.children('ul.children').length ){
				if( $_parent_li.hasClass('open') ){
					$_parent_li.removeClass('open'); 
					$_parent_li.children('ul.children').slideUp(150); 
				}else{
					$_parent_li.addClass('open'); 
					$_parent_li.children('ul.children').slideDown(150); 
				} 
			} 
		}); 

})(jQuery);