/** @file skeletor.js @author Sitefactory */ 

jQuery.noConflict(); (function($) {

	/* Enabloidaan strict mode */ 
	"use strict"; 

	var $dimmer = $('#mobilenav-dimmer'), $mobilenav = $('#mobilenav'); 
	var $_parent_li = 0, $_parent_ol = 0, $_this = 0; 

	$(document).on('click', '.toggle-mobilenav, #mobilenav-dimmer',  function(e) {

		e.preventDefault(); 

		/* check for active menus */
		$('.mobilenav-inner li.menu-item.menu-item-has-children[class*="current"]').addClass('open').children('ul.sub-menu').slideDown(150);

		if($dimmer.hasClass('show-dimmer')){
			$dimmer.fadeOut(300); 
			$mobilenav.fadeOut(200); 
		}else{
			$dimmer.fadeIn(350); $mobilenav.fadeIn(400); 
		} 

		$dimmer.toggleClass('show-dimmer'); $mobilenav.toggleClass('open'); 

		$('html').toggleClass('mobilenav-open');


	}).on('click', '#mobilenav .mobilenav-inner span.open-submenu', function(e){

		$_parent_li = $(this).parent();
		$_parent_ol = $(this).parent('ul');
		$_this = $(this); 

		if ( $_parent_li.children('ul.sub-menu').length ){
			if( $_parent_li.hasClass('open') ){
				$_parent_li.removeClass('open'); 
				/* $_this.removeClass('open'); 
				$_this.children('span.symbol').removeClass('symbol-minus').addClass('symbol-plus'); 
				$_parent_li.children('ul.sub-menu').removeClass('open'); */
				$_parent_li.children('ul.sub-menu').slideUp(150); 
			}else{
				$_parent_li.addClass('open'); 
				/* $_this.addClass('open'); 
				$_this.children('span.symbol').removeClass('symbol-plus').addClass('symbol-minus'); 
				$_parent_li.children('ul.sub-menu').addClass('open'); */
				$_parent_li.children('ul.sub-menu').slideDown(150); 
			} 
		} 
	}); 

	$('a.lightbox').colorbox({photo: true, opacity: .7, current: '{current} / {total}', maxHeight: '90%', maxWidth: '90%'/*,title: function(){var url = $(this).attr('href'); return '<a href="' + url + '" target="_blank">Lataa alkuperäisessä resoluutiossa</a>'; }*/ }); 

	/* Seuranta: <a href="http://www.example.com" onclick="trackOutboundLink('http://www.example.com'); return false;">Check out example.com</a> */ 
	var trackOutboundLink = function(url) {ga('send', 'event', 'outbound', 'click', url, {'hitCallback': function () { document.location = url; } }); }; 

})(jQuery);