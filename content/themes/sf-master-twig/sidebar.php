<?php
/**
 * The Template for displaying sidebar
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'sidebar.twig' ), $data, WPClass\WPSite::$default_template_cache_alive_time );
