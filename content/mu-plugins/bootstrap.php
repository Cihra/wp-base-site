<?php
/**
 * Plugin Name: Sitefactory Oy Bootstrap
 * Description: Includes library and class autoloader for using Sitefactory's themes and plugins
 */

spl_autoload_register('sitefactory_wp_autoloader');
function sitefactory_wp_autoloader ($class) {
	
	$segments = array_filter(explode("\\", $class));
	$first = array_shift($segments);
	
	//Load classes from mu-plugin
	if($first === "WPClass") {
		$path = plugin_dir_path(__FILE__)."sitefactory-wp/class/".implode("/",$segments).".class.php";

		if(file_exists($path))
			include_once $path;
	}

	//Load theme related classes
	elseif($first === "WPTheme") {


		$path = get_template_directory()."/app/".implode("/",$segments).".class.php";

		if(file_exists($path))
			include_once $path;
		else {
			$path = get_template_directory()."/app/".implode("/",$segments).".php";

			if(file_exists($path))
				include_once $path;
		}

	}
	
}

if ( file_exists( WP_CONTENT_DIR.'/mu-plugins/cmb2/init.php' ) ){
	require_once( WP_CONTENT_DIR.'/mu-plugins/cmb2/init.php' );
}
if ( file_exists( WP_CONTENT_DIR.'/mu-plugins/cmb/custom-meta-boxes.php' ) ){
	require_once( WP_CONTENT_DIR.'/mu-plugins/cmb/custom-meta-boxes.php' );
}
if ( file_exists( WP_CONTENT_DIR.'/mu-plugins/sitefactory-wp/sitefactory-wp.php' ) && file_exists( WP_CONTENT_DIR.'/mu-plugins/timber-library/timber.php' ) ){
	require_once( WP_CONTENT_DIR.'/mu-plugins/sitefactory-wp/sitefactory-wp.php' );
	require_once( WP_CONTENT_DIR.'/mu-plugins/timber-library/timber.php' );
}else{
	die('required files are missing: mu-plugins');
}