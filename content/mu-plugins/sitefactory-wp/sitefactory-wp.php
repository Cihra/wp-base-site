<?php 
/*
Plugin Name: Sitefactory Wordpress
Plugin URI: http://www.sitefactory.fi
Description: 
Author: Sitefactory Oy / Janne Martikainen
Version: 1.0
*/

// ************* DEFINES & BASIC SETTINGS *************
define("SITEFACTORY_WP_APP_DIR",plugin_dir_path( __FILE__ ).'');
define("SITEFACTORY_WP_CLASS_DIR",SITEFACTORY_WP_APP_DIR.'class');

// ************* REQUIREMENTS AND REQUIRING PLUGIN DEPENDENCIES *************

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!function_exists('cmb_init'))
	require_once SITEFACTORY_WP_APP_DIR.'/custom-meta-boxes/custom-meta-boxes.php';

if ( ! defined( 'WPINC' ) ) {
	die('No dirty business here!');
}
if ( defined( 'WP_INSTALLING' ) && WP_INSTALLING ) {
    return;
}
// Check if minimum requirements, WordPress 4.0.
include( ABSPATH . WPINC . '/version.php' ); // Include an unmodified $wp_version.
if ( version_compare( str_replace( '-src', '', $wp_version ), '4.0', '<' ) ) {
	// Show error notice to admins, if WP is not installed in the minimum required version, in which case CsvUpdater will not work.
	if ( current_user_can( 'update_plugins' ) ) {
		add_action( 'admin_notices', array( 'sitefactory-wp', 'show_minimum_requirements_error_notice' ) );
	}
	
	return;
}

add_filter('cmb_meta_boxes', function() {
	return WPClass\MetaBoxes::get();
});

/*
//Plugin main setting view
function sitewp_admin_links() {
	require_once SITEFACTORY_WP_TEMPLATE_DIR.'/main.php';
	add_menu_page( __( 'Quality Assurance Tool' ), __( 'QA Tool'), 'manage_options', SITEFACTORY_WP_TEMPLATE_DIR.'/main.php', 'siteqa_main_form', '', 999);
	add_action( 'admin_init', 'siteqa_register_settings' );
}
add_action('admin_menu','siteqa_admin_links');
*/
?>