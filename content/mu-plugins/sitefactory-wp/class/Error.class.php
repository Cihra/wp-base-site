<?php

namespace WPClass;
use WP_error as WP_Error;

Class Error {

	public static function get() {
		static $wp_error;
		return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}

	public static function show_error_messages() {
		if( $codes = Error::get() ) {
			$ret = '';
			//var_dump($codes); die();
			if(count($codes->errors)) {
				$ret .= '<div class="error-message">';
				    // Loop error codes and display errors
				   foreach($codes as $code){
				        $message = Error::get()->get_error_message($code);
				        if($message)
				     	   $ret .= '<span class="error"><strong>' . __('Virhe') . '</strong>: ' . $message . '</span><br/>';
				    }
				$ret .= '</div>';
				
				return $ret;
			}
		}

		return;
	}
	
}