<?php

namespace WPClass\Import;

Class Term {

	public $taxonomy;
	public $data;

	/**
     * @param string $taxonomy
     * @param array $data || Arrayn tulee sisältää argumentti-arrayta --> arrayssa oltava yhtenä keynä ainakin "term" --> muutoin ei yritetäkään
     */
	public function __construct($taxonomy, $data = array()) {

		$this->taxonomy = $taxonomy;
		$this->data = (array)$data;

		if(!$this->check())
			throw new \Exception('Tuonti epäonnistui - Tarkasta materiaalisi sekä tsekkaa, että taxonomy on olemassa, johon olet termejä lisäämässä.');
	}

	private function check() {
		if(count($this->data)) {
			if(taxonomy_exists($this->taxonomy))
				return true;
		}

		return false;
	}

 	/**
     * @todo Mahdollista myös muualta kuin lokaalista lähteestä importtaus
     */
	public function import() {

		$a_imported = array();
		set_time_limit(1200);

		if($this->taxonomy && is_array($this->data)) {
			foreach($this->data as $key => $args) {

				if(is_array($args)) {
					
					$default = array(
						'parent' => 0
					);

					$args = array_merge($default, $args);

					if($args['term']) {
						$term_name = $args['term'];
						unset($args['term']); 
						if($term_name)
							$a_imported[] = wp_insert_term($term_name, $this->taxonomy, $args);
					}
				}
			}
		}

		return $a_imported;
	}
}