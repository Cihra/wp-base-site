<?php

namespace WPClass\Import;

Class Post extends \WPClass\WPObject {

	static $_post_type = 'post';
	static $import_data;
	static $append_terms = true;
	public $taxonomies;

	/**
     * @param string $taxonomy
     * @param array $data || Arrayn tulee sisältää argumentti-arrayta --> arrayssa oltava yhtenä keynä ainakin "term" --> muutoin ei yritetäkään
     */
	public function __construct($node) {

		parent::__construct($node);
		$this->taxonomies = array();
	}

	/**
	 * Tarkastetaan:
	 * 1) Arrayssa on rivejä
	 * 2) Pakolliset sarakkeet löytyy
	 */
	private static function check() {
		if(!count( self::$import_data ))
			return false;

		if(!array_key_exists('name', self::$import_data[0]) && !array_key_exists('title', self::$import_data[0]))
			return false;

		return true;
	}	

	/**
     * 
     */
    public function save($data=null) {

        parent::save($data);

        //Liitetään taxonomyt
        if( count($this->taxonomies) ) {
	        foreach($this->taxonomies as $taxonomy_name => $terms) {
	        	$taxonomy_name = sanitize_title($taxonomy_name);
	            if( taxonomy_exists($taxonomy_name) && count((array)$terms) ) {
	                wp_set_object_terms($this->getID(), (array)$terms, $taxonomy_name, self::$append_terms);
	            }
	        }
	    }

   		return true;     
    }

 	/**
     * 
     */
	public static function import($post_type = 'post', $data = array()) {

		self::$_post_type = $post_type;
		self::$import_data = $data;

		if(!self::check())
			throw new \Exception('Tuonti epäonnistui - Tarkasta materiaalisi, että siellä on varmasti rivejä sekä sieltä löytyy WP:n tarvitsemat vähimmäis tiedot (name tai title).');

		$a_imported = array();
		set_time_limit(1200);

		if(is_array($data)) {
			foreach($data as $key => $args) {

				if(is_array($args)) {
					
					$new_post = new self($args);

					//update
					if($new_post->exists()) {
						foreach($args as $k => $value)
							$new_post->{$k} = $value;

					//Create
					} else {
						if(!isset($args['name']))
							$new_post->name = $args['title'];

						if(!isset($args['title']))
							$new_post->title = $args['name'];

						//taxonomyt
						//var_Dump($args); die();
						foreach($args as $k => $v) {
							if( strstr($k, 'taxonomy_') && !empty($k) ) {
								$new_post->taxonomies[str_replace('taxonomy_', '',sanitize_title($k))] = explode(",",$v);
							}
						}

					}
					//var_dump($new_post); die();

					$new_post->status = 'publish';
					if(!empty($args['status']))
						$new_post->status = $args['status'];

					$new_post->save();
						
					$a_imported[] = $new_post;
				}
			}
		}

		return $a_imported;
	}
}