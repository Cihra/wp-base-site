<?php

namespace WPClass;

use WPClass\MetaBoxes as MetaBoxes;
use TimberSite as TimberSite;
use Timber as Timber;
use TimberImage as TimberImage;
use Timber\ImageHelper as ImageHelper;
use Twig_Extension_StringLoader as Twig_Extension_StringLoader;
use Twig_Filter_Function as Twig_Filter_Function;

class WPSite extends TimberSite {

	/**
	 * Default time for transient cache
	 */ 
	public static $default_transient_cache_alive_time = CACHE_ALIVE_TIME_TRANSIENT;

	/**
	 * Default image thumbnail sizes for theme
	 */ 
	public static $default_template_cache_alive_time = CACHE_ALIVE_TIME_TIMBER;

	/**
	 * Environment for example for minified assets
	 */ 
	public static $environment = APP_ENV;

	/**
	 * Default image thumbnail sizes for theme
	 */ 
	public $default_thumbnail_sizes = array();

	/**
	 * Default time for transient cache
	 */ 
	public static $theme_slug = 'sitefactory-twig';

	function __construct() {

		$this->default_thumbnail_sizes = array();

		add_theme_support( 'post-formats', array() );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'menus' );
		
		add_filter( 'wp_nav_menu_args', array($this, 'set_default_menu_walker') ); 		//Default walker for menus
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		//add_action( 'init', array( $this, 'register_post_types' ) );
		//add_action( 'init', array( $this, 'register_taxonomies' ) );
		
		add_action('admin_init', array($this, 'wpb_imagelink_setup'), 10);					//Asettaa oletuksena, että kuvalla ei ole linkkiä
		add_filter('cmb_meta_boxes', array($this, "get_content_type_meta_boxes"));

		global $content_width;
		if ( ! isset( $content_width ) ){ 
			$content_width = 640;
		}

		//load_theme_textdomain( 'Sitefactory', get_template_directory() . '/languages' );
		//$this->register_sidebars();

		parent::__construct();
	}

	public function register_sidebars() {
		return;
	}

	/**
	 * @param string $sidebar
	 * 
	 */ 
	public function get_widgetarea($sidebar = null) {
		if(isset($sidebar))
			return Timber::get_widgets($sidebar);

		return false;
	}

	//Functions for twig in global context
	public function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( 'image', new Twig_Filter_Function( array($this,'image') ) );
		$twig->addFilter( 'get_language_navigation', new Twig_Filter_Function( array($this,'get_language_navigation') ) );
		$twig->addFilter( 'date_default', new Twig_Filter_Function( array($this,'date_get_default_format') ) );
		$twig->addFilter( 'date_long', new Twig_Filter_Function( array($this,'date_get_long_format') ) );
		$twig->addFilter( 'tel_link', new Twig_Filter_Function( array($this,'render_tel_link') ) );
		$twig->addFilter( 'responsive_image', new Twig_Filter_Function( array($this,'timber_image_to_responsive') ) );
		$twig->addFilter( 'wysiwyg', new Twig_Filter_Function( array($this,'wysiwyg_content_filter') ) );

		return $twig;
	}

	/**
	 * Asettaa, että kuvalla ei ole oletuksena mitään linkkiä
	 */
	public function imagelink_setup() {
		$image_set = get_option( 'image_default_link_type' );

		if ($image_set !== 'none') {
			update_option('image_default_link_type', 'none');
		}
	}


	public function get_content_type_meta_boxes() {
		return MetaBoxes::get();
	}

	/** 
	 * @param array $sizes | array including keys: name, width, height and crop (optional)
	 */ 
	public function set_thumbnail_sizes( $sizes = array() ) {
		
		foreach($sizes as $size) {

			if(isset($size['name']) &&
				isset($size['width']) &&
				isset($size['height'])) {
				add_image_size($size['name'], $size['width'], $size['height'], (boolean)$size['crop']);
			}
		}
	}

	/** 
	 * Sets default menu walker for all menus
	 * @author J3
	 * @todo / J3: siirrä tämä fiksumpaan paikkaan! 
	 */
	public function set_default_menu_walker( $args ) {
		
		if( empty($args['walker']) ) {
		    return array_merge( $args, array(
		        'walker' => new \WPTheme\MenuWalker\DefaultWalker()
		    ) );
		}

		return $args;
	}

	/**
	 * Return the image url or render image to screen
	 * Use in template: {{ variable_of_image|image(false,'my-seo-friendly-alt-text') }} 
	 * @author J3
	 */
	public function image($content, $render = false, $alt = '') {
		if(intval($content)) {
			$url = new TimberImage($content);

			if($render)
				return '<img src="'.$url.'" alt="'.$alt.'">';

			return $url;
		}
	}

	/** 
	 * Render TimberImage as responsive image with srcset and sizes attributes. Should be used just as a default responsive image fallback
	 * @author J3
	 * @param mixed $image
	 * @param string $alt
	 * @return string
	 */
	public function timber_image_to_responsive($image, $alt = null) {

		if(is_int($image) || is_string($image))
			$image = new Timber\Image(intval($image));

		if($image instanceof Timber\Image) {

			$ret = '';
			$cache_key = 'timber-image'.$image->src;
			$image_string_cache_alive_time = 3600 * 24; 

			$ret = get_transient($cache_key);
			$ret = false;
			if(!$ret) {
				$srcset = array();
				$sizes = '';

				foreach($this->default_thumbnail_sizes as $size) {
					if( intval($image->width) > intval($size['width']) ){
                    	$srcset[] = ImageHelper::resize($image->src,$size['width']).' '.$size['width'].'w';
					}
				}
                    
				if(!empty($srcset)) {
					$sizes = '(min-width: 1024px) 75vw';
					$srcset[] = $image->src.' '.$image->width.'w';
				}

				if(empty($alt))
					$alt = $image->caption;

				$ret = '<img src="'.$image->src.'" alt="'.$alt.'" srcset="'.implode(",",$srcset).'" sizes="'.$sizes.'">';
				set_transient($cache_key, $ret, $image_string_cache_alive_time);
			}

			return $ret;
		}

		return $image;
		
	}

	/** 
	 * Get current active language  (depends on WPML)
	 */ 
	public function get_current_language() {
		if( defined('ICL_LANGUAGE_CODE') )
			return ICL_LANGUAGE_CODE;

		return false;
	}

	/**
	 * Custom WPML language switcher
	 * @author J3
	 * @since 2016-04-22
	 * @param String $direction | vertical/horizontal
	 * @param int $display_mode | 1) Flags and language names, 2) only flags, 3) only language names, 4) only language prefix 
	 */
	public function get_language_navigation($direction='horizontal', $display_mode=1) {

		$ret = '<ul class="'.$direction.'">';
		if(function_exists('icl_get_languages')) {

			$lang_nav = icl_get_languages();
			if(is_array($lang_nav)) {
				foreach ($lang_nav as $lang) {
					$classes = mb_strtolower($lang['code']);
					if($lang['active'])
						$classes .= " active";
					
					$ret .= '<li class="'.$classes.'"><a href="'.$lang['url'].'">';
					switch($display_mode) {
						case 1 :
							$ret .= '<img src="'.get_template_directory_uri().'/assets/img/flags/'.mb_strtolower($lang['code']).'.png" alt="'.$lang['native_name'].'">';
							$ret .= mb_strtoupper($lang['native_name']);
							break;
						case 2 :
							$ret .= '<img src="'.get_template_directory_uri().'/assets/img/flags/'.mb_strtolower($lang['code']).'.png" alt="'.$lang['native_name'].'">';
							break;
						case 3 :
							$ret .= mb_strtoupper($lang['native_name']);
							break;
						case 4 :
							$ret .= mb_strtoupper($lang['code']);
							break;
					}
					$ret .= '</a></li>';
					
				}
			}
		}

		$ret .= '</ul>';

		return $ret;
	}

	/**
	 * Käännösmokkula sivupohjien kääntämiseen (koska ei voida käyttää mitään muita käännöstyökaluja multisite-asennuksessa).
	 * @author J3
	 * @created 2015-12-04
	 * 
	 */
	public function multi_translate($string = null, $print=false) {
		if($string) {

			$translated_string = __($string, WPSite::$theme_slug);

			//Return the translation if it was found (== different from the original)
			if($translated_string != $string) {
				if($print)
					echo $translated_string;
				else
					return $translated_string;
			}

			$current_language = $this->get_current_language();

			$blog_id = get_current_blog_id();
			$translation_file = get_template_directory().'/languages/'.$current_language;
			if(file_exists($translation_file)) {
				$lines = file($translation_file,FILE_SKIP_EMPTY_LINES);
				foreach($lines as $row) {
					$pattern = '/^'.trim(strip_tags($string)).'/';
					if (preg_match($pattern,$row)){
						$a = explode("=",$row);
						if($a[1])
							$string = trim($a[1]);
						break;	//Lasketaan kuormaa breikkaamalla ulos loopista
					}
				}
			}
		}

		if($print)
			echo $string;
		else
			return $string;
	}

	/**
	 * Käännösmokkula normisivulle
	 * @author Matu
	 * @created 2018-03-15
	 * 
	 */
	public function translate($string = null) {
		if($string) {
			return __($string, WPSite::$theme_slug);
		}
	}

	public static function searchform() {
		return get_search_form(false);
	}

	public static function show_error_messages() {
		return Error::show_error_messages();
	}

	/**
	 * Common date and time handling for templates
	 */
	public function date_get_default_format($content) {

		//parameter can be timestamp or some sort of date format
		if(!self::isValidTimeStamp($content))
			$content = strtotime($content);

		return date('d.m.Y',$content);
	}
	public function date_get_long_format($content) {

		//parameter can be timestamp or some sort of date format
		if(!self::isValidTimeStamp($content))
			$content = strtotime($content);
		
		return date('d.m.Y H:i',$content);
	}
	
	public static function isValidTimeStamp($timestamp) {
	    return ((string) (int) $timestamp === $timestamp) 
	        && ($timestamp <= PHP_INT_MAX)
	        && ($timestamp >= ~PHP_INT_MAX);
	}

	public function render_tel_link($content, $class=null, $itemdrop=null) {
		if(isset($class))
			$class = ' class="'.$class.'"';

		if(isset($itemdrop))
			$itemdrop = ' itemdrop="'.$itemdrop.'"';

		$tel = preg_replace('/\d{3}([().-\s[\]]*)\d{3}([().-\s[\]]*)\d{4}/', "123$1456$27890",trim($content));

		$first_letter = substr($tel, 0, 1);
        if( !in_array($first_letter, array('(', '+', '0')) ) {
            $tel = '0'.$tel;
            $content = '0'.$content;
        }

        return '<a href="tel:'.str_replace(" ","",$tel).'"'.$class.$itemdrop.'>'.$content.'</a>';
	}

	/**
	 * @author J3
	 * @since 2016-02-25
	 * Muutetaan tekstin kokoa sanapituuksien perusteella hankalissa paikoissa, joissa pitkät sanat rikkovat ulkoasua
	 **/
	public function text_size_adjust($string) {
		
		//sanapituudet ja luokat
		$a_rule = array(
			'text_adjust_small' => 15
		);

		foreach(explode(" ",$string) as $word) {
			foreach($a_rule as $class => $limit) {
				if(strlen($word) >= $limit) {
					$string = str_replace($word,'<span class="'.$class.'">'.$word.'</span>', $string);
					break;
				}
			}
		}

		return $string;
	}

	/**
	 *
	 */
	public function wysiwyg_content_filter($content) {
		return apply_filters( 'the_content', $content );
	}

	/**
	 * @author Juuz, Matu
	 * @since 2016-11-28
	 * Voidaan hakea helposti posteja
	 * - Lisätty attachment -ehto
	 **/
	public static function get_posts_by_post_type($post_type = "post", $post_per_page = '-1', $orderby = 'date', $order = 'DESC', $taxonomy=false, $taxonomy_term=false){
	    $status = 'publish';
        if( $post_type == "attachment" ){
            $status = 'inherit';
        }
        $args = array(
            'post_type' =>  $post_type,
            'posts_per_page' => $post_per_page, 
            'orderby' => $orderby, 
            'order' => $order,
            'post_status' => $status,
            'suppress_filters' => false 
            );

        if($taxonomy && $taxonomy_term){
            if( is_numeric($taxonomy_term) || is_array($taxonomy_term) ){
                $args['tax_query'] = array( array( 'taxonomy' => $taxonomy, 'field' => 'id', 'terms' => $taxonomy_term) );
            }else{
                $args['tax_query'] = array( array( 'taxonomy' => $taxonomy, 'field' => 'slug', 'terms' => $taxonomy_term) );
            }
        }

        return Timber::get_posts( $args );
    }
}