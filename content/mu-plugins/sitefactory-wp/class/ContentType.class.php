<?php
/**
 * @author Janne Martikainen
 * - martikainen.janne@gmail.com
 * - http://www.jannemartikainen.net
 * 
 * @link https://codex.wordpress.org/Function_Reference/register_post_type
 * @version 0.1
 * 
 */

namespace WPClass;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class ContentType {

	public $type;
	public $options = array();
	public $labels = array();
	public $meta_boxes = array();


	public function __construct($type, $options = array(), $labels = array(), $meta_boxes = array()) {
		
		$this->type = $type;
		$default_options = array(
			'public' => true,
			'supports' => array('title','editor','revision','thumbnail')
		);

		$required_labels = array(
			'singular_name' => ucwords($this->type),
			'plural_name' => ucwords($this->type)
		);

		$this->options = $options + $default_options;
		$this->labels = $labels + $required_labels;

		$this->options['labels'] = $this->labels + $this->default_labels();

		if(is_array($meta_boxes)) {
			$this->meta_boxes = $meta_boxes;
		}

		add_action('init',array($this,"register"));
		MetaBoxes::register_meta_boxes($this->meta_boxes);
	}

	public function register() {
		register_post_type($this->type,$this->options);
	}

	public function default_labels() {

		return array(
			'name' => $this->labels['plural_name'],
			'singular_name' => $this->labels['singular_name'],
			'add_new' => 'Add New '. $this->labels['singular_name'],
			'add_new_item' => 'Add New '.$this->labels['singular_name'],
			'edit' => 'Edit',
			'edit_item' => 'Edit '.$this->labels['singular_name'],
			'new_item' => 'New '.$this->labels['singular_name'],
			'view' => 'View '.$this->labels['singular_name'],
			'view_item' => 'View '.$this->labels['singular_name'],
			'search_items' => 'Search '.$this->labels['plural_name'],
			'not_found' => 'No matching '.strtolower($this->labels['plural_name']. ' found'),
			'not_found_in_trash' => 'No matching '.strtolower($this->labels['plural_name']. ' found in trash'),
			'parent_item_colon' => 'Parent '.$this->labels['singular_name']
		);

	}

	public function get_meta_boxes($meta_boxes = array()) {
		if(is_array($meta_boxes) && count((array)$meta_boxes))
			$this->meta_boxes = $meta_boxes;
		
		return $this->meta_boxes;
	}


}

//$quotes = new ContentType('quote',[],['plural_name' => 'Quotes']);
/*
add_action('init', function() {
	$options = [
		'labels' => [],
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => ['slug' => 'book'],
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => ['title','editor','author','thumbnail','excerpt','comments']
	];
	
});*/