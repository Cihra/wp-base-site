<?php
/**
 * @author Janne Martikainen
 * - martikainen.janne@gmail.com
 * - http://www.jannemartikainen.net
 * @version 0.1
 */

namespace WPClass;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class WPObject {

	static $_post_type = 'post';
	protected $data;

	/* Array containing meta keys and values to update */
	protected $a_update_post_meta;

	/**
	 * @param mixed $wp_post | WP Object / Array / Post_ID
	 */
	public function __construct($wp_post = null) {

		$defaults = array(
		  'ID'             => '', 		// Are you updating an existing post?''
		  'post_content'   => '', 		// The full text of the post.
		  'post_name'      => '', 		// The name (slug) for your post
		  'post_title'     => '', 		// The title of your post.
		  'post_status'    => '', 		// [ 'draft' | 'publish' | 'pending'| 'future' | 'private' | custom registered status ] // Default 'draft'.
		  'post_type'      => 'post', 		//[ 'post' | 'page' | 'link' | 'nav_menu_item' | custom post type ] // Default 'post'.
		  'post_author'    => '', 		//[ <user ID> ] // The user ID number of the author. Default is the current user ID.
		  'ping_status'    => '', 		//[ 'closed' | 'open' ] // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'.
		  'post_parent'    => '', 		// Sets the parent of the new post, if any. Default 0.
		  'menu_order'     => '', 		// If new post is a page, sets the order in which it should appear in supported menus. Default 0.
		  'to_ping'        => '', 		// Space or carriage return-separated list of URLs to ping. Default empty string.
		  'pinged'         => '', 		// Space or carriage return-separated list of URLs that have been pinged. Default empty string.
		  'post_password'  => '', 		// Password for post, if any. Default empty string.
		  //'guid'           => 			// Skip this and let Wordpress handle it, usually.
		  'post_content_filtered' => '', 	// Skip this and let Wordpress handle it, usually.
		  'post_excerpt'   => '', 		// For all your post excerpt needs.
		  'post_date'      => '', 		// The time post was made.
		  'post_date_gmt'  => '', 		// The time post was made, in GMT.
		  'post_modified'	=> '', 		//Post modified date
		  'comment_status' => '', 		//[ 'closed' | 'open' ] // Default is the option 'default_comment_status', or 'closed'.
		  'post_category'  => '', 		// Default empty.
		  'tags_input'     => '', 		// Default empty.
		  'tax_input'      => '', 		//[ array( <taxonomy> => <array | string>, <taxonomy_other> => <array | string> ) ] // For custom taxonomies. Default empty.
		  'page_template'  => '' 		//[ <string> ] // Requires name of template file, eg template.php. Default empty.
		);

		$this->data = $defaults;

		$defaults['post_type'] = static::$_post_type;

		//wp_post argument can be either id, object or array (we don't need to do anything)
		if(is_int($wp_post) || is_string($wp_post))
			$wp_post = $this->_convert_object_to_array(get_post(intval($wp_post)));
		elseif(is_object($wp_post)) 
			$wp_post = $this->_convert_object_to_array($wp_post);

		if(is_array($wp_post))
			$this->data = array_merge($defaults, $wp_post);

		$this->a_update_post_meta = array();
		
		//Loop through the default data property and assign all the "not default wordpress" properties to meta fields
		foreach($this->data as $k => $v) {
			$k = trim($k);
			if( !array_key_exists($k, $defaults) && !array_key_exists('post_'.$k, $defaults) )
				$this->a_update_post_meta[$k] = $v;
		}

	}

	/**
	 * Just a separate accessory method for returning the object id
	 */ 
	public function getID() {
		return intval($this->data['ID']);
	}

	/** 
	 * 
	 */ 
	public function __get($name=null) {
		
		if(is_array($this->data)) {
			$name = mb_strtolower(trim($name));

			if($name) {
				if(array_key_exists($name,$this->data))
					return $this->data[$name];
				elseif(array_key_exists('post_'.$name,$this->data))
					return $this->data['post_'.$name];
				elseif(array_key_exists($name,$this->a_update_post_meta))
					return $this->a_update_post_meta[$name];
				else {
					$temp = get_post_meta( intval($this->data['ID']), $name );
					return $temp;
				}
			}
		}

		return;
	}

	/**
	 * 
	 */
	public function __set($name,$value) {

		$name = mb_strtolower(trim($name));

		if(is_array($this->data)) {
			if($name) {
				if(array_key_exists($name,$this->data))
					$this->data[$name] = $value;
				elseif(array_key_exists('post_'.$name,$this->data))
					$this->data['post_'.$name] = $value;
				elseif(!property_exists(get_class($this),$name)) {
					$this->a_update_post_meta[$name] = $value;
				}
			}
		}
	}

	/**
	 * Check if object is already saved to db and therefore "exists"
	 * @return boolean
	 */ 
	public function __isset($name) {

		$name = mb_strtolower($name);

	    if(array_key_exists($name, $this->data))
	    	return true;
	    elseif(array_key_exists('post_'.$name,$this->data))
	    	return true;
    	else {
			$temp = get_post_meta( intval($this->data['ID']), $name, true );
			if($temp !== false)
				return true;
		}

	    return false;
	}


	/**
	 * Check if object is already saved to db and therefore "exists"
	 * @return boolean
	 */ 
	public function exists() {
		if($this->data['ID']) 
			return true;

		return false;
	}

	/**
	 * get permalink of object
	 */
	public function getLink() {
		return get_permalink($this->data['ID']);
	}

	/**
	 * get custom excerpt of post
	 */
	public function getExcerpt() {
		if(empty($this->data['post_excerpt'])) {
			$this->data['post_excerpt'] = substr(strip_tags(trim($this->data['post_content'])),0,200).'...';
		}

		return $this->data['post_excerpt'];
	}

	/**
	 * get custom excerpt of post
	 * @return mixed WPAttachment or id
	 */
	public function getFeaturedImage($thumbnail_size = false) {

		$thumb_id = get_post_thumbnail_id($this->getId());

		if($thumb_id) {
			if( class_exists('WPClass\Attachment') ) {
				return new Attachment(intval($thumb_id), $thumbnail_size);
			} else {
				$thumb_array = wp_get_attachment_image_src($thumb_id, $thumbnail_size);
				return $thumb_array[0];
			}
		}
	}

	/**
	 * Get terms of this post by taxonomy-name
	 * @param string|array $taxonomy
	 */
	public function getTerms($taxonomy = null, $args = null) {
		if($taxonomy)
			return wp_get_post_terms($this->data['ID'],$taxonomy,$args);
	}

	/**
	 * Get single post by id or param
	 * @param int/array $param Wordpress post id
	 * @return WPObject
	 */
	public static function find($param) {

		if(is_int($param)) {
			$object = get_post($param);
			$wpobject = new static($object);
			//var_dump($wpobject);
		} else {
			$defaults = array(
				'posts_per_page'   => 1,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => '',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => static::$_post_type,
				'post_mime_type'   => '',
				'post_parent'      => '',
				'author' 			=> '',
				'post_status'      => 'publish',
				'suppress_filters' => false 
			);
			
			$param = array_merge($defaults,$param);

			$ret = get_posts($param);
			$wpobject = new static($ret);	//Let's take the first one from the array
		}
		return $wpobject;
	}

	/**
	 * Get list of posts by search params (similiar to Wordpress get_posts -function)
	 * @param array $args List of parameters for finding the object set
	 * @return array WPObject instances
	 */
	public static function search($_args = array()) {

		//Defaults
		$defaults = array(
			'posts_per_page'   => 5,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			//'meta_key'         => '',
			//'meta_value'       => '',
			'post_type'        => static::$_post_type,
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => false 
		);

		$args = array_merge($defaults,$_args);

		//print_r($args);
		//print_r(get_posts($args)); die();

		$ret = array();
		foreach(get_posts($args) as $object) {
			$ret[] = new static($object);
		}

		return $ret;
		
		/*return array_map(function ($object) {
			return new static($object);
		}, get_posts($args));*/
	}

	/**
	 * @param array $data
	 * @return  WPObject
	 */
	public function create($data = null) {

		if(is_array($data))
			$this->data = array_merge($this->data,$data);

		unset($data['ID']);

		$new_post_id = wp_insert_post($this->data);

		//Save meta fields
		foreach($this->a_update_post_meta as $k => $v) {
			update_post_meta(intval($new_post_id),$k,$v);
		}

		/*if(is_wp_error($errors)) {
			var_dump($errors->get_error_message());
		}*/

		return $object;
	}

	/**
	 * Save object to database. Updates existing or creates a new one
	 */
	public function save($data = null) {

		if(is_array($data))
			$this->data = array_merge($this->data,$data);

		//Update or create post if necessary
		if($this->data['ID'])
			wp_update_post($this->data,true);
		else {
			//var_dump($this->data); die();
			$new_post_id = wp_insert_post($this->data);
			$this->data['ID'] = $new_post_id;
		}

		//Update meta fields
		foreach($this->a_update_post_meta as $k => $v) {
			update_post_meta(intval($this->data['ID']),$k,$v);
		}

		/*if(is_wp_error($errors)) {
			var_dump($errors->get_error_message());
		}*/

		return $this;
	}

	/**
	 * 
	 * @return  WPObject
	 */
	public function delete() {
		wp_delete_post($this->data['ID']);

		return $this;
	}

	private static function _convert_object_to_array($object) {

		$ret = array();
		foreach($object as $k => $v) {
			$ret[$k] = $v;
		}

		return $ret;

	}

	private static function fix_value($string, $filter=null) {

		/* Use wordpress filters */
		if($filter)
			return apply_filters($filter,$string);

		return trim($string);
	}
}