<?php
/**
 * Version: 1.0
 * Author: Janne Martikainen / Sitefactory Oy
 * License: GPL2
 * @since 2016-09-16
 */

namespace WPClass;

use WP_Widget as WP_Widget;
use Timber as Timber;

/**
 * Class Widget_Better_Starter_Widget
 */
class WidgetBootstrap extends WP_Widget {

	/** Basic Widget Settings */
	public $widget_name;
	public $widget_description;
	public $textdomain;
	public $template;
	public $fields;

	private $context;

	/**
	 * Construct the widget
	 */
	public function __construct() {

		//We're going to use $this->textdomain as both the translation domain and the widget class name and ID
		$this->textdomain = strtolower(get_called_class());

		//Default template
		$this->context = array();
		$this->template = array( 'parts/widget-'.$this->textdomain.'.twig', 'parts/widget.twig', 'index.twig' );

		if( !empty($this->textdomain) && !empty($this->widget_name) && !empty($this->widget_description) ) {

			//Default fields for all widgets
			$this->add_field(
				'title',
				array(
					'name' => 'title', 
					'description' => 'Title', 
					'default_value' => '', 
					'type' => 'text'
				)
			);
			
			//Translations
			load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages' );
		
			//Init the widget
			parent::__construct(
				$this->textdomain,
				__($this->widget_name, $this->textdomain),
				array( 
					'description' => __($this->widget_description, $this->textdomain), 
					'classname' => $this->textdomain
				)
			);

			add_action( 'widgets_init', array($this, 'init_widget') );

		} else
			die("You must set name, description and class name for the object!");
	}

	public function init_widget() {
		$called_class = get_class($this);
		if($called_class)
			register_widget( $called_class );
	}

	/**********************************************************************************
	 * Widget frontend
	 *
	 * @param array $args
	 * @param array $instance
	 ******************************************************************************** */
	public function widget($args, $instance) {

		$title = apply_filters('widget_title', $instance['title']);

		$context = Timber::get_context();
		$context['before_widget'] = $args['before_widget'];
		$context['title'] = $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		$context['after_widget'] = $args['after_widget'];
		$context['additional_classes'] = sanitize_title($this->textdomain);

		foreach($this->context as $key => $_data) {
			$context[sanitize_title($key)] = $_data;
		}

		Timber::render( $this->template, $context, WPSite::$default_template_cache_alive_time );
	}
	

	/*********************************************************************************
	 * Widget backend
	 *
	 * @param array $instance
	 * @return string|void
	 ******************************************************************************** */
	public function form( $instance ) {

		foreach($this->fields as $field_name => $field_data) {
			
			switch( $field_data['type'] ) {

				case 'text' :
					echo '<p><label for="'. $this->get_field_id($field_name) .'">'. __($field_data['description'], $this->textdomain ). '</label>'.
					'<input class="widefat" id="'. $this->get_field_id($field_name) .'" name="'. $this->get_field_name($field_name) .'" type="text" value="'. esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : $field_data['default_value']) .'" /></p>';
					break;

				case 'textarea' :
					echo '<p><label for="'. $this->get_field_id($field_name) .'">'. __($field_data['description'], $this->textdomain ). '</label>'.
					'<textarea class="widefat" id="'. $this->get_field_id($field_name) .'" name="'. $this->get_field_name($field_name) .'">'.
					 esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : $field_data['default_value']). '</textarea></p>';
					break;

				case 'select' :
					$ret = '<p><label for="'. $this->get_field_id($field_name) .'">'. __($field_data['description'], $this->textdomain ). '</label>'.
					'<select class="widefat" id="'. $this->get_field_id($field_name) .'" name="'. $this->get_field_name($field_name) .'">';

					if( count($field_data['options']) ) {
						foreach($field_data['options'] as $value => $option_name) {
							$selected = '';
							if( ($value == $instance[$field_name]) || ( empty($instance[$field_name]) && ($field_data['default_value'] == $instance[$field_name]) ) )
								$selected = ' selected="selected"';
							$ret .= '<option value="'.$value.'"'.$selected.'>'.$option_name.'</option>';
						}
					}

					$ret .= '</select></p>';

					echo $ret;

					break;

				default :
					echo __('Error - Field type not supported', $this->textdomain) . ': ' . $field_data['type'];
			}
				
		}

	}

	/*********************************************************************************
	 * Updating widget by replacing the old instance with new
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 ******************************************************************************** */
	public function update( $new_instance, $old_instance ) {

		$instance = array();
		foreach($this->fields as $k => $field) {
			$instance[$k] = ( ! empty( $new_instance[$k] ) ) ? strip_tags( $new_instance[$k] ) : '';
		}
		
		return $instance;
	}

	/**
	 * Adds a text field to the widget
	 *
	 * @param string $field_name
	 * @param array $args
	 */
	protected function add_field( $field_name, $args = array() ) {

		$field_name = sanitize_title($field_name);

		if(!is_array($this->fields))
			$this->fields = array();

		if(is_array($args)) {
			$defaults = array(
				'name' => $field_name, 
				'description' => '', 
				'default_value' => '', 
				'type' => $field_type = 'text',
				'options' => array()
			);

			$this->fields[$field_name] = array_merge($defaults, $args);
		}

	}

	/**
	 * Adds variables to context
	 *
	 * @param string $variable_name
	 * @param mixed $data
	 *
	 */
	protected function add_context($variable_name, $data = null) {
		if($variable_name)
			$this->context[$variable_name] = $data;
	}

	/**
	 * Adds template to twig template array (to beginning of the priority)
	 *
	 * @param string $template_name
	 *
	 */
	protected function add_template($template_name) {
		$this->template = array_unshift($this->template, $template_name);
	}

}
?>