<?php
/**
 * Välimuisti-olio - Esittää aina yhden välimuistitettavan elementin. Toimii joko tiedostopohjaisena tai käyttää APC:ta
 * 
 * Pyritty rakentamaan sillä tavalla, että käyttö toimii samaan tapaan kuin vanhassakin cache-luokassa
 *
 * @author J3
 * @since 2014-08-29
 */

namespace WPClass;

if ( ! class_exists( 'J3Cache' ) ) {

	define("J3_CACHE_TEMP_DIR",PUBLIC_ROOT.'/temp/');

	class J3Cache
	{

		/**
		 * Hakemisto, johon cache-tiedostot tallennetaan
		 * 
		 * @var string
		 */
		static $dir;

		/**
		 * Tiedoston nimi, joka sisältää cachetetun datan
		 * 
		 * @var string
		 */
		private $file_name;

		/**
		 * Etuliite, jolla voidaan kertoa cachetettavan asian nimi (esim. modulin nimi, noston nimi tms.)
		 * 
		 * @var string
		 */
		private $prefix;

		/**
		 * Sekunteina cachen säilyvyys
		 * 
		 * @var int | seconds
		 */
		private $expire;
		
		/**
		 * Cache-tiedoston nimi
		 * 
		 * @var string
		 */
		private $name;


		/**
		 * Välimuistitettava data.
		 * 
		 * @var string
		 */
		private $data;
		
		/**
		 * Cachen tyyppi.
		 * Vaihtoehtoja:
		 * - file | Tiedostopohjainen cache
		 * - apc | Oliotyyppien tallennus APC-cacheen
		 * 
		 * @var string
		 */
		private $mode;

		/**
		 * Merkki, jolla limitoidaan
		 */
		private $file_name_delimiter;
		private $file_prefix = '.cache';

		
		/**
		 * Luokan konstruktori
		 * @param $prefix | Tunniste luotavalle cache-tiedostolle
		 * @param $expire | Sekunneissa miten pitkän aikaa cache on voimassa (poistettu toistaiseksi käytöstä)
		 * @param $mode | disk|apc 
		 *
		 */
		public function __construct($prefix = null, $expire = 0, $mode = null) {
			
			if(isset($prefix))
				$this->setPrefix($prefix);
			if(isset($expire))
				$this->setExpire($expire);
			else
				$this->setExpire(12800);				//default expire time
			if(isset($mode))
				$this->getMode($mode);
			else
				$this->setMode('disk');				//Oletuksena käytetään tiedostopohjaista cachea


			$this->setFileNameDelimiter('--');	
			$this->setName();
			self::$dir = J3_CACHE_TEMP_DIR.'cache/';

			//Jos annettiin expire-arvo, niin tsekataan ettei cache-tiedosto ole jo vanha
			if(intval($expire) && file_exists($this->getFileName())) {
				if($this->getCacheFileAge() >= $expire ) {
					//kakku on vanha --> poistetaan
					unlink($this->getFileName());
				}
			}

		}

		/* Aksessori-metodit */
		public function getPrefix() { return $this->prefix; }
		public function setPrefix($data) { $this->prefix = $data; }
		public function getExpire() { return $this->expire; }
		public function setExpire($data) { $this->expire = $data; }
		public function getMode() { return $this->mode; }
		public function setMode($data) { $this->mode = $data; }
		public function getFileNameDelimiter() { return $this->file_name_delimiter; }
		public function setFileNameDelimiter($data) { $this->file_name_delimiter = $data; }

		public function getFileName() { 
			return self::$dir.$this->name.$this->file_prefix;
		}

		public function getCacheFileAge() { 
			return time()-filemtime(self::$dir.$this->name.$this->file_prefix);
		}

		/**
		 * Haetaan cache-filun nimi
		 * @return string
		 */
		public function getName() {
			return self::sanitateKey($this->name);
		}

		/**
		 * Muodostetaan cache-filun nimi
		 */
		public function setName() {
			$this->name = $this->getPrefix();
			//expiroituminen otettu toistaiseksi vielä pois: .$this->getFileNameDelimiter().strtotime("+".intval($this->expire).' seconds')
		}

		/**
		 * Palauttaa cachen
		 * 
		 */
		public function get(){
			//Kyselemättä palautetaan aina luokkaan tallennettu data, jolla nopeutetaan hieman luokan käyttöä, ettei tarvitse aina lukea dataa erikseen
			if(isset($this->data))
				return $this->data;

			//Luetaan cachea
			switch ($this->getMode()){

				case 'disk':

					if(file_exists($this->getFileName())) {

						$ret = file_get_contents($this->getFileName());
						$this->data = unserialize(base64_decode($ret));
						if($this->data === false)
	    					$this->data = $ret;
					}

					break;
				case 'apc':

					$this->data = apc_fetch($this->getName());
					break;
			}
			
			return $this->data;
		}

		/**
		 * Asettaa cachen
		 * @param $data | Cacheen tallennettava tieto
		 */
		public function set($data){

			switch ($this->getMode()){

				case 'disk':

					if (!is_dir(self::$dir))
				    	mkdir(self::$dir);

					$this->data = is_string($data) 
			    		? preg_replace('/>\s+</', '><', $data) 
			    		: base64_encode(serialize($data));


					//Jos dataa löytyy, niin poistellaan vanha filu ja luodaan uusi
					if($this->data) {

						unlink($this->getFileName());
						
				    	$f = fopen($this->getFileName(), "wa+");
			    		
				    	if ($f) {
				      		fwrite($f, $this->data);
				      		fclose($f);
				      		chmod($file, 0775);
				    	}
					 	
					}

					break;

				case 'apc':

					apc_store($this->getName(), $data, $this->getExpire());
					break;
			}
			
		}

		/**
		 * Poistaa cachetiedoston
		 */
		public function delete() {
			switch($this->getMode()) {
		        
	    		case 'file' :
	    			unlink($this->getFileName());
	    			break;
	    		case 'apc' :
					apc_delete($this->getName());
					break;
			}
		}

		/**
	     * Tyhjentää kaikki cache-tiedostot. Toimii vain tiedoistoille, ei APC:lle. Voidaan tietyllä nimiavaruudella poistaa vain tietyn nimeämisen sisältävät tiedostot
	     * @param $namespace | Poistetaan vain tiedostot, jotka sisältävät tiedostonimessään tämän määritellyn termin
	     */
	    public static function clearAllCaches($namespace = null){

			$handle = opendir(self::$dir);

			$failSafe = 1000;
			$i=0;
			while ($file = basename(readdir($handle))) {
			    if($file != '.' && $file != '..') {
			        if (strpos($file, '.cache') !== false){
			        	if (strstr($file, $namespace) || !isset($namespace)) {
			            	$file = self::$dir . $file;
			                if (file_exists($file)) {
			                     unlink($file);
			                }
			            }
			        }
			    }
			    $i++;
			}

	    }


	     /**
	     * Poimii cachetiedostot etuliitteen perusteella
	     * @todo ei toimi oikein, tarkasta miksi olion arvot muuttuvat oliota luotaessa
	     * @param string $prefix Etuliite jonka mukaan cachetiedostot haetaan
	     * @return array Lista cacheista
	     */
	    function get_by_prefix($prefix, $namespace = false){
	        $prefix = self::sanitateKey($prefix);
	        $handle = opendir($this->_dir);
	        $a_ret  = array();

			while ($file = basename(readdir($handle))) {
			    if (strpos($file, '.cache') !== false && 
			        (
			        	(
			        		strpos($file, $this->_namespace . $prefix) === 0 && !$namespace
			        	) ||
			        	(
			        		strpos($file, $prefix) === 0 && $namespace
			        	)
			        )
			    ){
		            list($namespace, $name, $expire, $age) = explode('-', str_replace('.cache', '', $file));
		            $cache = new cache($name);

		            $cache->set_namespace($namespace);
		            $cache->set_expiration($age);
		            
		            if ($cache->exists()){
		            	
			            $a_ret[] = $cache;
		            } 
			    }
			}
			
			return $a_ret;
	    }
		
		
		
		/**
		 * Siivotaan datan avaimeksi asetettavaa merkkijonoa.
		 * 
		 * @param string $key Datan avain
		 * @return string Siivottu avain
		 */
		public static function sanitateKey($key){
			return trim(stripslashes($key));
		}

	}
}