<?php

namespace WPClass;

Class Request {

	public static function get_ajax_url($nonce = '') {
		return admin_url( 'admin-ajax.php?nonce='.self::create_nonce($nonce) );
	}

	public static function create_nonce($nonce_name = 'extremely_secret_nonce_name') {
		return wp_create_nonce($nonce_name);
	}

	/**
	 * @uses ajax
	 */
	public static function check_nonce($nonce = null, $nonce_name = 'extremely_secret_nonce_name') {

		if(!isset($nonce))
			$nonce = $_REQUEST['nonce'];

		if ( wp_verify_nonce( $nonce, $nonce_name))
			return true;

		return false;
	}

}