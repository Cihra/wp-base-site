<?php

namespace WPClass;

Class Csv {

	public $filename;
	public $delimiter;
	public $charset;
	public $rows;
	public $columns;

	/**
     * @param string $filename
     * @param string $delimiter
     * @param string $charset
     * @param array $columns
     * @param array $rows
     */
	public function __construct($filename, $delimiter = ';', $charset = 'utf-8', $columns = array(), $rows = array()) {

		if($filename) {
			$this->filename = $filename;
			$this->delimiter = $delimiter;
			$this->charset = $charset;
			$this->columns = $columns;
			$this->rows = $rows;

			$this->readFile();
		} else
			throw new \Exception('Annatko tiedostonimen luokan konstruktorissa, jotta voidaan jotain dataa yrittää lukea :)');
	}

 	/**
     * @todo Mahdollista myös muualta kuin lokaalista lähteestä importtaus
     */
	public function readFile() {

		$a_imported = array();
		set_time_limit(1200);


		//Lokaalin tiedoston importaus
		if( is_file($this->filename) ) {

			$a_file = file($this->filename);

			//Jos ei annettu sarakkeita, niin oletetaan että ne tulee filessä mukana
			if(!count($this->columns)) {				
				$this->columns = explode($this->delimiter,$a_file[0]);
				unset($a_file[0]);
			}


			if(is_array($a_file) ) {

				foreach($a_file as $i => $_row ) {

					//haetaan rivitiedot ja mäpätään column-nimiin
					$rowdata = explode($this->delimiter, $_row);
					
					$row = array();

					//Useampi sarake
					if(count($this->columns)) {
						foreach($this->columns as $k => $c) {
							if( !empty($rowdata[$k]) && $c)
								$row[trim($c)] = self::prepareString($rowdata[$k]);
						}
						$a_imported[] = $row;

					//Vain yksisarake
					} else
						$a_imported[] = $_row; 
					

				}
			}

		}

		$this->rows = $a_imported;

		return $a_imported;

	}

	/**
	 * @param string $string
	 */
	public static function prepareString($string) {
		return Toolbox\pl_String::forceUTF8(stripslashes(trim($string)));
	}
}