<?php
/**
 * Helper functions to strings
 *  
 * @since 2017-02-03
 * @author J3
 */

namespace WPClass\Toolbox;

Class pl_String {

	/**
	 * Pakotetaan utf-8
	 * @param string $input
	 * @return string
	 */
	public static function forceUTF8($input) {
    
	    // From http://w3.org/International/questions/qa-forms-utf-8.html
	    if(!preg_match('%^(?:
	          [\x09\x0A\x0D\x20-\x7E]            # ASCII
	        | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
	        |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
	        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
	        |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
	        |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
	        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
	        |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
	    )*$%xs', $input))
	    	return utf8_encode($input);

    return $input;
    
	}

}