<?php
/**
 * Helper class for handling attachments in WP
 *  
 * @since 2017-02-03
 * @author J3
 */

namespace WPClass\Toolbox;

Class Array {

	/**
	 * Assosiatiivisten arrayden splice-funtio
	 * @param array &$input
	 * @param int $offset
	 * @param int $lenght
	 * @param array $replacement
	 * @return void
	 */
	public static function array_splice_assoc(&$input, $offset, $length, $replacement) {
        $replacement = (array) $replacement;
        $key_indices = array_flip(array_keys($input));
        if (isset($input[$offset]) && is_string($offset)) {
                $offset = $key_indices[$offset];
        }
        if (isset($input[$length]) && is_string($length)) {
                $length = $key_indices[$length] - $offset;
        }

        $input = array_slice($input, 0, $offset, TRUE)
                + $replacement
                + array_slice($input, $offset + $length, NULL, TRUE);
	}

	/**
	 * Liikuttele assosiatiivisessä arrayssa nodeja
	 * @param string $which | key
	 * @param int $where | offset
	 * @param array $array | source array
	 * @return array
	 */
	public static function array_move($which, $where, $array) {
	    $tmpWhich = $which;
	    $j=0;
	    $keys = array_keys($array);

	    for($i=0;$i<count($array);$i++)
	    {
	        if($keys[$i]==$tmpWhich)
	            $tmpWhich = $j;
	        else
	            $j++;
	    }
	    $tmp  = array_splice($array, $tmpWhich, 1);
	    self::array_splice_assoc($array, $where, 0, $tmp);
	    return $array;
	}

}