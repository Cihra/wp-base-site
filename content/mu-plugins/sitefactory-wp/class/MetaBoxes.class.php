<?php
/**
 * @author Janne Martikainen
 * - martikainen.janne@gmail.com
 * - http://www.jannemartikainen.net
 * @version 0.1
 * @since 2016-02-10
 */

namespace WPClass;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class MetaBoxes {

	static public $instance;
	static protected $data = array();		//Holds all the metaboxes of different content types

	private function __construct() {

	}

	static public function get() {
		return self::$data;
	}

	static public function register_meta_boxes($meta_boxes = array()) {
		if(is_array($meta_boxes) && count((array)$meta_boxes)) {
			self::$data[] = $meta_boxes;
		}
	}

	static public function getInstance() {

	}
}