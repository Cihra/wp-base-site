<?php
define('WP_CACHE', true);

define('WP_FOLDER_NAME','/wordpress');
define('WP_ROOT', dirname( __FILE__ ) . WP_FOLDER_NAME);

$app_env = getenv('APP_ENV');

if(!$app_env)
	$app_env = strtolower(getenv('LOCAL_ENVIRONMENT'));

define('APP_ENV',$app_env);

require dirname( __FILE__ ) . '/config/configurations.php';/* Enviroment settings */

if ( !defined('ABSPATH') )
	define('ABSPATH', WP_ROOT . '/');
require_once(WP_ROOT . '/wp-settings.php');
